<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Color;
use App\Models\Size;
use Illuminate\Database\Seeder;
use Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Category::insert([
            [
                'title' => 'Sản phẩm mới',
                'slug' => Str::slug('Sản phẩm mới'),
            ],
            [
                'title' => 'Đầm',
                'slug' => Str::slug('Đầm')
            ],
            [
                'title' => 'Áo sơ mi',
                'slug' => Str::slug('Áo sơ mi')
            ],
            [
                'title' => 'Chân váy công sở',
                'slug' => Str::slug('Chân váy công sở')
            ],
            [
                'title' => 'Quần âu',
                'slug' => Str::slug('Quần âu')
            ],
            [
                'title' => 'Bộ công sở',
                'slug' => Str::slug('Bộ công sở')
            ],
            [
                'title' => 'Măng tô',
                'slug' => Str::slug('Măng tô')
            ],
            [
                'title' => 'Vest nữ',
                'slug' => Str::slug('Vest nữ')
            ],
        ]);
//        Size::insert([
//            [
//                'title' => 'S',
//            ],
//            [
//                'title' => 'M',
//            ],
//            [
//                'title' => 'L',
//            ],
//            [
//                'title' => 'XL',
//            ],
//        ]);
    }
}
