<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id')->nullable();
            $table->string('title');
            $table->string('slug');
            $table->string('sku')->nullable();
            $table->boolean('gender')->default(0)->nullable()->comment('0:nữ|1:nam');
            $table->enum('season', ['xuan', 'ha', 'thu', 'dong'])->nullable();
            $table->string('image')->nullable()->comment('ảnh đại diện');
            $table->string('trademark')->default('Đang cập nhật')->comment('thương hiệu');
            $table->double('price')->nullable();
            $table->smallInteger('sale_off')->default(0)->comment('% sale off , từ 0 -> 100');
            $table->integer('solds')->default(0)->comment('số sản phẩm đã bán');
            $table->string('material')->nullable()->comment('chất liệu');
            $table->text('detail')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
