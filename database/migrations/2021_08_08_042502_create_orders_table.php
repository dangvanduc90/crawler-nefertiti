<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('customer_id')->nullable();
            $table->string('email');
            $table->string('billing_name');
            $table->string('billing_phone');
            $table->text('billing_address')->nullable();
            $table->integer('billing_province_id')->nullable();
            $table->integer('billing_district_id')->nullable();
            $table->integer('billing_ward_id')->nullable();
            $table->text('note')->nullable();
            $table->boolean('shipping_method')->nullable();
            $table->boolean('payment_method')->nullable();
            $table->string('discount_code')->nullable();
            $table->string('code')->nullable();
            $table->smallInteger('payment_status')->nullable();
            $table->smallInteger('process_status')->nullable();
            $table->smallInteger('state')->nullable();
            $table->text('client_info')->nullable();
            $table->double('total_price');
            $table->integer('total_quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
