<?php

namespace App\Scraper;

use App\Models\Color;
use App\Models\Post;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductVariant;
use App\Models\Size;
use Goutte\Client;
use Illuminate\Support\Facades\Log;
use Symfony\Component\BrowserKit\HttpBrowser;
use Symfony\Component\DomCrawler\Crawler;

class Nefertiti
{

    public function scrape()
    {
        $base_url = 'https://nefertiti.com.vn';
        $client = new Client();

        for ($page = 1; $page <= 34; $page++) {
            $url = "{$base_url}/collections/all?q=&page={$page}&view=grid";
            $crawler = $client->request('GET', $url);
            $crawler->filter('.evo-product-block-item')->each(
                function (Crawler $node) use ($base_url, $client) {
                    $product_url = $base_url . $node->filter('.product_name h4 a')->attr('href');
                    $crawlerProduct = $client->request('GET', $product_url);

                    try {
                        $titleProduct = $crawlerProduct->filter('.title-head')->first()->text();
                        $skuProduct = $crawlerProduct->filter('.variant-sku')->first()->text();
                        $priceProduct = $crawlerProduct->filter('.product-price')->first()->text();
                        $priceProduct = str_replace(['.', '₫'], ['', ''], $priceProduct);

                        $imagesProduct = $crawlerProduct->filter('.slider-for a')->each(function (Crawler $node) {
                            return 'https:' . $node->attr('href');
                        });

                        $materialProduct = null;
                        try {
                            $materialProduct = $crawlerProduct->filter('#variant-swatch-2')->first()->text();
                        } catch (\Exception $ex) {

                        }

                        $product = Product::create([
                            'title' => $titleProduct,
                            'sku' => $skuProduct,
                            'slug' => \Str::slug($titleProduct),
                            'image' => $imagesProduct[0],
                            'price' => $priceProduct,
                            'material' => $materialProduct,
                        ]);

                        $rowImages = [];
                        foreach ($imagesProduct as $image) {
                            $rowImages[] = [
                                'product_id' => $product->id,
                                'image' => $image,
                            ];
                        }
                        ProductImage::insert($rowImages);

                        $_sizes = $crawlerProduct->filter('#variant-swatch-0 .select-swap > div')->each(function (Crawler $node) {
                            return $node->attr('data-value');
                        });
                        $_colours = $crawlerProduct->filter('#variant-swatch-1 .select-swap .no-thumb')->each(function (Crawler $node) {
                            return $node->filter('span')->text();
                        });

                        $rows = [];
                        if (count($_sizes) > 0 && count($_colours) > 0) {
                            foreach ($_sizes as $_size) {
                                foreach ($_colours as $_color) {
                                    $size = Size::firstOrCreate(['title' => $_size]);
                                    $color = Color::firstOrCreate(['title' => $_color]);
                                    $rows[] = [
                                        'product_id' => $product->id,
                                        'size_id' => $size->id,
                                        'color_id' => $color->id,
                                        'quantity' => 100,
                                    ];
                                }
                            }
                        } else {
                            if (count($_colours) > 0) {
                                foreach ($_colours as $_color) {
                                    $color = Color::firstOrCreate(['title' => $_color]);
                                    $rows[] = [
                                        'product_id' => $product->id,
                                        'size_id' => null,
                                        'color_id' => $color->id,
                                        'quantity' => 100,
                                    ];
                                }
                            } else if (count($_sizes) > 0) {
                                foreach ($_sizes as $_size) {
                                    $size = Size::firstOrCreate(['title' => $_size]);
                                    $rows[] = [
                                        'product_id' => $product->id,
                                        'size_id' => $size->id,
                                        'color_id' => null,
                                        'quantity' => 100,
                                    ];
                                }
                            } else {
                                $rows[] = [
                                    'product_id' => $product->id,
                                    'size_id' => null,
                                    'color_id' => null,
                                    'quantity' => 100,
                                ];
                            }
                        }
                        ProductVariant::insert($rows);

                    } catch (\Exception $ex) {
                        Log::warning($ex->getMessage());
                    }
                }
            );
        }
    }

    public function scrapeCategory()
    {
        $base_url = 'https://nefertiti.com.vn';
        $client = new Client();

        $url = "{$base_url}/thoi-trang-nam";
        $crawler = $client->request('GET', $url);
        $crawler->filter('.evo-product-block-item')->each(
            function (Crawler $node) use ($base_url, $client) {
                $product_title = $node->filter('.product_name h4 a')->text();
                try {
                    Product::updateOrCreate(['title' => $product_title], ['gender' => 1]);
                } catch (\Exception $ex) {
                    Log::warning($product_title . '::' . $ex->getMessage());
                }
            }
        );
        $url = "{$base_url}/thoi-trang-nam?q=collections:2598177&page=2&view=grid";
        $crawler = $client->request('GET', $url);
        $crawler->filter('.evo-product-block-item')->each(
            function (Crawler $node) use ($base_url, $client) {
                $product_title = $node->filter('.product_name h4 a')->text();
                try {
                    Product::updateOrCreate(['title' => $product_title], ['gender' => 1]);
                } catch (\Exception $ex) {
                    Log::warning($product_title . '::' . $ex->getMessage());
                }
            }
        );

        for ($page = 1; $page <= 1; $page++) {
            $url = "{$base_url}/san-pham-moi-1";
            $crawler = $client->request('GET', $url);
            $crawler->filter('.evo-product-block-item')->each(
                function (Crawler $node) use ($base_url, $client, $page) {
                    $product_title = $node->filter('.product_name h4 a')->text();
                    try {
                        Product::updateOrCreate(['title' => $product_title], ['category_id' => 1]);
                    } catch (\Exception $ex) {
                        Log::warning($product_title . '::' . $ex->getMessage());
                    }
                }
            );
        }
        for ($page = 1; $page <= 1; $page++) {
            $url = "{$base_url}/dam";
            $crawler = $client->request('GET', $url);
            $crawler->filter('.evo-product-block-item')->each(
                function (Crawler $node) use ($base_url, $client, $page) {
                    $product_title = $node->filter('.product_name h4 a')->text();
                    try {
                        Product::updateOrCreate(['title' => $product_title], ['category_id' => 2]);
                    } catch (\Exception $ex) {
                        Log::warning($product_title . '::' . $ex->getMessage());
                    }
                }
            );
        }
        for ($page = 1; $page <= 1; $page++) {
            $url = "{$base_url}/ao-so-mi";
            $crawler = $client->request('GET', $url);
            $crawler->filter('.evo-product-block-item')->each(
                function (Crawler $node) use ($base_url, $client, $page) {
                    $product_title = $node->filter('.product_name h4 a')->text();
                    try {
                        Product::updateOrCreate(['title' => $product_title], ['category_id' => 3]);
                    } catch (\Exception $ex) {
                        Log::warning($product_title . '::' . $ex->getMessage());
                    }
                }
            );
        }
        for ($page = 1; $page <= 1; $page++) {
            $url = "{$base_url}/chan-vay";
            $crawler = $client->request('GET', $url);
            $crawler->filter('.evo-product-block-item')->each(
                function (Crawler $node) use ($base_url, $client, $page) {
                    $product_title = $node->filter('.product_name h4 a')->text();
                    try {
                        Product::updateOrCreate(['title' => $product_title], ['category_id' => 4]);
                    } catch (\Exception $ex) {
                        Log::warning($product_title . '::' . $ex->getMessage());
                    }
                }
            );
        }
        for ($page = 1; $page <= 1; $page++) {
            $url = "{$base_url}/collections/all?q=product_type.filter_key:(\"Hàng%20Xuân\")&page={$page}&view=grid";
            $crawler = $client->request('GET', $url);
            $crawler->filter('.evo-product-block-item')->each(
                function (Crawler $node) use ($base_url, $client, $page) {
                    $product_title = $node->filter('.product_name h4 a')->text();
                    try {
                        Product::updateOrCreate(['title' => $product_title], ['season' => 'xuan']);
                    } catch (\Exception $ex) {
                        Log::warning($product_title . '::' . $ex->getMessage());
                    }
                }
            );
        }
        for ($page = 1; $page <= 12; $page++) {
            $url = "{$base_url}/collections/all?q=product_type.filter_key:(\"Hàng%20hè\")&page={$page}&view=grid";
            $crawler = $client->request('GET', $url);
            $crawler->filter('.evo-product-block-item')->each(
                function (Crawler $node) use ($base_url, $client, $page) {
                    $product_title = $node->filter('.product_name h4 a')->text();
                    try {
                        Product::updateOrCreate(['title' => $product_title], ['season' => 'ha']);
                    } catch (\Exception $ex) {
                        Log::warning($product_title . '::' . $ex->getMessage());
                    }
                }
            );
        }
        for ($page = 1; $page <= 12; $page++) {
            $url = "{$base_url}/collections/all?q=product_type.filter_key:(\"Hàng%20thu\")&page={$page}&view=grid";
            $crawler = $client->request('GET', $url);
            $crawler->filter('.evo-product-block-item')->each(
                function (Crawler $node) use ($base_url, $client, $page) {
                    $product_title = $node->filter('.product_name h4 a')->text();
                    try {
                        Product::updateOrCreate(['title' => $product_title], ['season' => 'thu']);
                    } catch (\Exception $ex) {
                        Log::warning($product_title . '::' . $ex->getMessage());
                    }
                }
            );
        }
        for ($page = 1; $page <= 10; $page++) {
            $url = "{$base_url}/collections/all?q=product_type.filter_key:(\"Hàng%20đông\")&page={$page}&view=grid";
            $crawler = $client->request('GET', $url);
            $crawler->filter('.evo-product-block-item')->each(
                function (Crawler $node) use ($base_url, $client, $page) {
                    $product_title = $node->filter('.product_name h4 a')->text();
                    try {
                        Product::updateOrCreate(['title' => $product_title], ['season' => 'dong']);
                    } catch (\Exception $ex) {
                        Log::warning($product_title . '::' . $ex->getMessage());
                    }
                }
            );
        }
    }


    public function scrapePost()
    {
        $base_url = 'https://nefertiti.com.vn';
        $client = new Client();

        for ($page = 1; $page <= 4; $page++) {
            $url = "{$base_url}/tin-tuc?page={$page}";
            $crawler = $client->request('GET', $url);
            $crawler->filter('.blog-item')->each(
                function (Crawler $node) use ($base_url, $client) {
                    $post_title = $node->filter('.blog-item-name a')->first()->attr('title');
                    $post_image = 'https:' . $node->filter('.blog-item-thumbnail img')->first()->attr('data-src');
                    $post_description = $node->filter('.blog-item-summary')->first()->text();

                    $post_url = $node->filter('.blog-item-name a')->attr('href');
                    $crawlerPost = $client->request('GET', $base_url . $post_url);
                    $post_content = $crawlerPost->filter('.evo-toc-content')->first()->html();

                    Post::create([
                        'title' => $post_title,
                        'slug' => trim($post_url, '/'),
                        'image' => $post_image,
                        'description' => $post_description,
                        'content' => $post_content
                    ]);
                }
            );
        }
    }
}
