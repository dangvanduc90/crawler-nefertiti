<?php

namespace App\Http\Controllers;

use App\Models\Product;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->get('query');
        $products = Product::where('title', 'like', "%{$keyword}%")->paginate();
        Breadcrumbs::register('search', function ($trail) {
            $trail->parent('home');
            $trail->push('Tìm kiếm sản phẩm', url()->current());
        });
        return view('product.index', compact('products'));
    }
}
