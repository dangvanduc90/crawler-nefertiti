<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $productBySeason = Product::where('season', 'thu')->limit(4)->get();
        return view('home', compact('productBySeason'));
    }
}
