<?php

namespace App\Http\Controllers;

use App\Models\Category;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function show(Request $request, Category $category)
    {
        if ($request->query('view') === 'ajaxload4') {
            $products = $category->products()->limit(4)->get();
            return view('product.list_ajax', compact('products'));
        } elseif ($request->query('view') === 'ajaxload') {
            $products = $category->products()->get();
            return view('product.list_ajax', compact('products'));
        } else {
            $products = $category->products()->paginate();
            Breadcrumbs::register('category.show', function ($trail) use ($category) {
                $trail->parent('home');
                $trail->push($category->title, url()->current());
            });
            return view('product.index', compact('products'));
        }
    }
}
