<?php

namespace App\Http\Controllers;

use App\Models\Ward;
use Illuminate\Http\Request;

class WardController extends Controller
{

    public function getWardByDistrictId($district_id)
    {
        return response()->json([
            'success_flag' => true,
            'data' => Ward::where('district_id', $district_id)->get()
        ]);
    }
}
