<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderItem;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Cookie;
use Log;

class OrderController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = Order::create(array_merge($request->all(), [
            'total_price' => Cart::total(),
            'total_quantity' => Cart::count(),
        ]));
        $phone = $request->get('billing_phone');
        $email = $request->get('email');
        $name = $request->get('billing_name');
        $address = $request->get('billing_address');
        $shipping_method = $request->get('shipping_method');
        $payment_method = $request->get('payment_method');
        $province_id = $request->get('province_id');
        $district_id = $request->get('district_id');
        $ward_id = $request->get('ward_id');
        $note = $request->get('note');
        $customer_id = Customer::updateOrCreate([
            'phone' => $phone
        ], [
            'email' => $email,
            'name' => $name,
            'address' => $address,
            'shipping_method' => $shipping_method,
            'payment_method' => $payment_method,
            'province_id' => $province_id,
            'district_id' => $district_id,
            'ward_id' => $ward_id,
            'note' => $note,
        ]);

        Cookie::make('customer_id', $customer_id);
        Cookie::make('email', $email);
        Cookie::make('billing_name', $name);
        Cookie::make('billing_phone', $phone);
        Cookie::make('billing_address', $address);
        Cookie::make('shipping_method', $shipping_method);
        Cookie::make('payment_method', $payment_method);
        Cookie::make('note', $note);

        foreach (Cart::content() as $cart) {
            $row = [];
            $row['order_id'] = $order->id;
            $row['product_variant_id'] = $cart->id;
            $row['price'] = $cart->price;
            $row['quantity'] = $cart->qty;
            $row['created_at'] = date('Y-m-d H:i:s');
            $row['updated_at'] = date('Y-m-d H:i:s');
            try {
                OrderItem::create($row);
                Cart::remove($cart->rowId);
            } catch (\Exception $ex) {
                Log::error($ex->getMessage());
            }
        }
        return redirect()->route('order.thank.you', $order->code);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        Breadcrumbs::register('order.show', function ($trail) {
            $trail->parent('home');
            $trail->push('Chi tiết đơn hàng', url()->current());
        });
        $order_items = $order->items;
        return view('order.show', compact('order', 'order_items'));
    }

    public function thankYou(Order $order)
    {
        return view('order.thank_you', compact('order'));
    }
}
