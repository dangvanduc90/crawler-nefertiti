<?php

namespace App\Http\Controllers;

use App\Models\Province;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provinces = Province::all();
        return view('checkout.index', compact('provinces'));
    }
}
