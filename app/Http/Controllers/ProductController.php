<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductVariant;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        Breadcrumbs::register('product.index', function ($trail) {
            $trail->parent('home');
            $trail->push('Tất cả sản phẩm', url()->current());
        });
        $products = Product::paginate();
        return view('product.index', compact('products'));
    }

    public function show(Product $product, Request $request)
    {
        Breadcrumbs::register('product.show', function ($trail) use ($product) {
            $trail->parent('home');
            if ($product->category) {
                $trail->push($product->category->title, route('category.show', $product->category->slug));
            }
            $trail->push($product->title, url()->current());
        });
        $category = $product->category;
        if ($request->get('variant')) {
            $variant = ProductVariant::findOrFail($request->get('variant'));
        } else {
            $variant = null;
        }
        $productRelated = Product::where('category_id', $product->category_id)
            ->where('id', '!=', $product->id)
            ->limit(5)
            ->orderByRaw('season', '=', $product->season)
            ->orderByDesc('id', )
            ->get();
        return view('product.show', compact('product', 'productRelated', 'category', 'variant'));
    }

    public function forMen()
    {
        Breadcrumbs::register('product.for.men', function ($trail) {
            $trail->parent('home');
            $trail->push('Tất cả sản phẩm', route('product.index'));
            $trail->push('Thời trang nam', url()->current());
        });
        $products = Product::where('gender', 1)->paginate();
        return view('product.index', compact('products'));
    }

    public function saleOff(Request $request)
    {
        Breadcrumbs::register('product.sale.off', function ($trail) {
            $trail->parent('home');
            $trail->push('Tất cả sản phẩm', route('product.index'));
            $trail->push('Sale off', url()->current());
        });
        $products = Product::where('sale_off', '!=', 0);

        if ($request->ajax() || $request->wantsJson()) {
            if ($request->query('view') === 'ajaxload4') {
                $products = $products->limit(4)->get();
                return view('product.list_ajax', compact('products'));
            } elseif ($request->query('view') === 'ajaxload') {
                $products = $products->get();
                return view('product.list_ajax', compact('products'));
            }
            return view('product.list_ajax', compact('products'));
        }
        $products = $products->paginate();
        return view('product.index', compact('products'));
    }
}
