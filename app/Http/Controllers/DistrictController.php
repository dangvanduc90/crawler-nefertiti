<?php

namespace App\Http\Controllers;

use App\Models\District;
use Illuminate\Http\Request;

class DistrictController extends Controller
{
    public function getDistrictByProvinceId($province_id)
    {
        return response()->json([
            'success_flag' => true,
            'data' => District::where('province_id', $province_id)->get()
        ]);
    }
}
