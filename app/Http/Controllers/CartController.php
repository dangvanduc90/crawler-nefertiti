<?php

namespace App\Http\Controllers;

use App\Models\ProductVariant;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Exception;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Breadcrumbs::register('cart.index', function ($trail) {
            $trail->parent('home');
            $trail->push('Giỏ hàng', url()->current());
        });
        return view('cart.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $variant = ProductVariant::where('product_id', $request->get('product_id'))
            ->where('size_id', $request->get('size_id'))
            ->where('color_id', $request->get('color_id'))
            ->first();
        if (!$variant) {
            echo 'Not found variant';
            die();
        }
        Cart::add(
            intval($variant->id),
            $request->get('product_title'),
            intval($request->get('quantity')),
            $request->get('product_price'),
            [
                'size_id' => $request->get('size_id'),
                'color_id' => $request->get('color_id'),
            ]
        )->associate(new ProductVariant());
        return redirect()->route('cart.index');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $rowId)
    {
        $success_flag = true;
        $msg = '';
        try {
            Cart::update($rowId, intval($request->get('quantity')));
        } catch (Exception $ex) {
            $success_flag = false;
            $msg = $ex->getMessage();
        }
        if ($request->ajax() || $request->wantsJson()) {
            return response()->json([
                'success_flag' => $success_flag,
                'msg' => $msg,
            ]);
        }
        return redirect()->route('cart.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $rowId
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $rowId)
    {
        $success_flag = true;
        $msg = '';
        try {
            Cart::remove($rowId);
        } catch (Exception $ex) {
            $success_flag = false;
            $msg = $ex->getMessage();
        }
        if ($request->ajax() || $request->wantsJson()) {
            return response()->json([
                'success_flag' => $success_flag,
                'msg' => $msg,
            ]);
        }
        return redirect()->route('cart.index');
    }

    public function repurchaseItem(Request $request)
    {
        $this->store($request);
    }

    public function repurchaseOrder(Request $request)
    {
        $items = $request->except('_token');
        foreach ($items as $item) {
            $variant_id = $item['variant_id'];
            $product_title = $item['product_title'];
            $quantity = $item['quantity'];
            $product_price = $item['product_price'];
            $size_id = $item['size_id'];
            $color_id = $item['color_id'];
            Cart::add(
                intval($variant_id),
                $product_title,
                intval($quantity),
                $product_price,
                [
                    'size_id' => $size_id,
                    'color_id' => $color_id,
                ]
            )->associate(new ProductVariant());
        }
        return redirect()->route('cart.index');
    }
}
