<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Breadcrumbs::register('post.index', function ($trail) {
            $trail->parent('home');
            $trail->push('Tin tức', url()->current());
        });
        $posts = Post::paginate(4);
        return view('post.index', compact('posts'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        Breadcrumbs::register('post.show', function ($trail) use ($post) {
            $trail->parent('home');
            $trail->push('Tin tức', route('post.index'));
            $trail->push($post->title, url()->current());
        });
        $categories = Category::all();
        $relatePosts = Post::where('id', '!=', $post->id)->limit(5)->get();
        return view('post.show', compact('post', 'categories', 'relatePosts'));
    }

}
