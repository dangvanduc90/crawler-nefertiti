<?php

namespace App\Observers;

use App\Models\Order;

class OrderObserver
{
    /**
     * Handle the Order "created" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function creating(Order $order)
    {
        $order->code = md5($order->customer_id . '_' . time());
        $order->client_info = json_encode([
            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
            'ip' => getIPAddress(),
            'REQUEST' => $_REQUEST,
            'device_type' => getBrowser()->device->type,
            'os' => getBrowser()->os->name ? trim(getBrowser()->os->name) : '',
            'browser' => getBrowser()->browser->name ? trim(getBrowser()->browser->name) : '',
        ]);
    }
}
