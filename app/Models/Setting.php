<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * Class Setting
 * @package App\Models
 * @property integer id
 * @property string name
 * @property string value
 */
class Setting extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'value'
    ];

    public static function get($field)
    {
        $setting = Setting::where('name', $field)->first();
        if (!$setting) {
            return null;
        }
        return self::castValue($setting->value, $setting->type);
    }

    public static function set($field, $value, $type = 'string')
    {
        return Setting::updateOrCreate(['name' => $field], ['value' => $value, 'type' => $type]);
    }

    public static function remove($field)
    {
        Setting::where('name', $field)->delete();
    }

    /**
     * caste value into respective type
     *
     * @param $val
     * @param $castTo
     * @return bool|int|float
     */
    private static function castValue($val, $castTo)
    {
        switch ($castTo) {
            case 'int':
            case 'integer':
                return intval($val);
            case 'bool':
            case 'boolean':
                return boolval($val);
            case 'float':
                return floatval($val);
            case 'double':
                return doubleval($val);
            default:
                return $val;
        }
    }
}
