<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Str;

/**
 * Class OrderItem
 * @package App\Models
 * @property integer id
 * @property integer order_id
 * @property integer product_variant_id
 * @property integer price
 * @property integer quantity
 * @property string payment_status
 * @property string process_status
 * @property string tracking_url
 * @property string created_at
 * @property string updated_at
 */
class OrderItem extends Model
{
    use HasFactory;
    protected $fillable = [
        'order_id',
        'product_variant_id',
        'price',
        'quantity',
        'payment_status',
        'process_status',
        'tracking_url',
    ];

    public function product_variant()
    {
        return $this->belongsTo(ProductVariant::class);
    }
}
