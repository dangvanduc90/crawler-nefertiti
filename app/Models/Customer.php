<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Customer
 * @package App\Models
 * @property string phone
 * @property string email
 * @property string name
 * @property string address
 * @property string province_id
 * @property integer district_id
 * @property string ward_id
 * @property string note
 * @property string shipping_method
 * @property string payment_method
 */
class Customer extends Model
{
    use HasFactory;
    protected $fillable = [
        'phone',
        'email',
        'name',
        'address',
        'province_id',
        'district_id',
        'ward_id',
        'note',
        'shipping_method',
        'payment_method',
    ];

    protected $casts = [
        'shipping_method' => 'boolean',
        'payment_status' => 'boolean',
    ];
}
