<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App\Models
 * @property integer id
 * @property integer parent_id
 * @property string title
 * @property string slug
 * @property string sku
 * @property string created_at
 * @property string updated_at
 * @property object products
 */

class Category extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function getRouteKeyName(){
        return 'slug';
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
