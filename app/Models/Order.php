<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * Class Order
 * @package App\Models
 * @property integer id
 * @property integer customer_id
 * @property string billing_name
 * @property string billing_phone
 * @property string billing_address
 * @property string billing_province_id
 * @property integer billing_district_id
 * @property string billing_ward_id
 * @property string note
 * @property string shipping_method
 * @property integer payment_method
 * @property string discount_code
 * @property string code
 * @property integer payment_status
 * @property integer process_status
 * @property integer state
 * @property string client_info
 * @property integer total_price
 * @property integer total_quantity
 * @property string created_at
 * @property string updated_at
 */

class Order extends Model
{
    use HasFactory;
    protected $fillable = [
        'customer_id',
        'email',
        'billing_name',
        'billing_phone',
        'billing_address',
        'billing_province_id',
        'billing_district_id',
        'billing_ward_id',
        'note',
        'shipping_method',
        'payment_method',
        'discount_code',
        'code',
        'payment_status',
        'process_status',
        'state',
        'client_info',
        'total_price',
        'total_quantity',
    ];

    protected $casts = [
        'shipping_method' => 'boolean',
        'payment_status' => 'boolean',
    ];

    public function getRouteKeyName(){
        return 'code';
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }
}
