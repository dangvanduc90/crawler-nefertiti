<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Models
 * @property integer id
 * @property integer category_id
 * @property string title
 * @property string slug
 * @property string sku
 * @property string season
 * @property integer price
 * @property string image
 * @property string trademark
 * @property string material
 * @property Category category
 * @property string created_at
 * @property string updated_at
 */

class Product extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $perPage = 24;

    public function getRouteKeyName(){
        return 'slug';
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function sizes()
    {
        return $this->belongsToMany(Size::class, 'product_variants')->groupBy('size_id')
            ->select(['sizes.id', 'sizes.title'])
            ->groupBy(['sizes.id', 'sizes.title', 'pivot_product_id', 'pivot_size_id']);
    }

    public function colours()
    {
        return $this->belongsToMany(Color::class, 'product_variants')
            ->select(['colors.id', 'colors.title', 'colors.hex_code'])
            ->groupBy(['colors.id', 'colors.title', 'colors.hex_code', 'pivot_product_id', 'pivot_color_id']);
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function pluck_images($char = '')
    {
        $images = [];
        foreach ($this->images as $product_image) {
            $images[] = $char . $product_image->image . $char;
        }
        return $images;
    }

}
