<?php

namespace App\Models;

use Gloudemans\Shoppingcart\Contracts\Buyable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Str;

/**
 * Class ProductVariant
 * @package App\Models
 * @property integer id
 * @property integer product_id
 * @property integer size_id
 * @property integer color_id
 */
class ProductVariant extends Model implements Buyable
{
    use HasFactory;
    protected $guarded = [];
    protected $with = ['product', 'size', 'color'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function size()
    {
        return $this->belongsTo(Size::class);
    }

    public function color()
    {
        return $this->belongsTo(Color::class);
    }

    public function getAttrValuesAttribute()
    {
        $attr_values = [];
        if ($this->size) {
            $attr_values[] = Str::ucfirst($this->size->title);
        }
        if ($this->color) {
            $attr_values[] = Str::ucfirst($this->color->title);
        }
        if ($this->product) {
            $attr_values[] = Str::ucfirst($this->product->material);
        }
        return implode(' / ', $attr_values);
    }

    public function getAttrUrlAttribute()
    {
        return route('product.show', $this->product->slug) . '?variant=' . $this->id;
    }

    public function getBuyableIdentifier($options = null)
    {
        return $this->id;
    }

    /**
     * Get the description or title of the Buyable item.
     *
     * @return string
     */
    public function getBuyableDescription($options = null)
    {
        return $this->product->detail;
    }

    /**
     * Get the price of the Buyable item.
     *
     * @return float
     */
    public function getBuyablePrice($options = null)
    {
        return $this->product->price;
    }
}
