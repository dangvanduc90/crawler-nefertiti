<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Post
 * @package App\Models
 * @property integer id
 * @property string title
 * @property string slug
 * @property string image
 * @property string description
 * @property string content
 */
class Post extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'slug',
        'image',
        'description',
        'content',
    ];

    public function getRouteKeyName(){
        return 'slug';
    }
}
