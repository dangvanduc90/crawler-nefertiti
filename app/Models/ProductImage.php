<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductImage
 * @package App\Models
 * @property integer id
 * @property integer product_id
 * @property string image
 */
class ProductImage extends Model
{
    use HasFactory;
    protected $guarded = [];
}
