<?php
use App\Models\Setting;

if (!function_exists('formatPrice')) {
    function formatPrice($price, $currency = '₫')
    {
        return number_format(doubleval($price), 0, ',', '.') . $currency;
    }
}

if (! function_exists('setting')) {
    /**
     * @param string|array $key
     * @param null $default
     * @return Setting|bool|float|int|mixed
     */
    function setting($key, $default = null)
    {
        if (is_array($key)) {
            try {
                Setting::set($key[0], $key[1]);
            } catch (Exception $ex) {
                Log::error($ex->getMessage());
                dd($ex->getMessage());
            }
        }

        $value = Setting::get($key);
        return is_null($value) ? value($default) : $value;
    }
}

if (! function_exists('getBrowser')) {
    function getBrowser()
    {
        static $info = null;

        if ($info === null) {
            $info = new WhichBrowser\Parser(getallheaders());
        }

        return $info;
    }
}

if (! function_exists('getIPAddress')) {
    function getIPAddress() {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip_address = $_SERVER['HTTP_CLIENT_IP'];
        }
        //whether ip is from proxy
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        //whether ip is from remote address
        else
        {
            $ip_address = $_SERVER['REMOTE_ADDR'];
        }
        return $ip_address;
    }
}
