@extends('layouts.app')
@section('title', 'Giỏ hàng')
@section('head')
    <link href='{{ asset('/css/evo-carts.scss.css') }}' rel='stylesheet'>
@endsection
@section('body')
    {{ Breadcrumbs::render() }}
    <div class="container white collections-container margin-bottom-20 margin-top-30">
        <div class="white-background">
            <div class="row">
                <div class="col-md-12">
                    <div class="shopping-cart">
                        <div class="visible-md visible-lg">
                            <div class="shopping-cart-table">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h1 class="lbl-shopping-cart lbl-shopping-cart-gio-hang">Giỏ hàng <span>(<span
                                                    class="count_item_pr">{{ Cart::count() }}</span> sản phẩm)</span></h1>
                                    </div>
                                </div>
                                <div class="row">
                                    @if(Cart::count() > 0)
                                    <div class="col-main cart_desktop_page cart-page">
                                        <form id="shopping-cart" action="{{ route('cart.index') }}" method="post" novalidate>
                                            <div class="cart page_cart cart_des_page hidden-xs-down">
                                                <div class="col-xs-9 cart-col-1">
                                                    <div class="cart-tbody">
                                                        @foreach(Cart::content() as $cart)
                                                        <div class="row shopping-cart-item productid-{{ $cart->rowId }}">
                                                            <div class="col-xs-3 img-thumnail-custom"><p class="image">
                                                                    <a href="{{ $cart->model->attr_url }}"
                                                                       title="{{ $cart->model->product->title }} - {{ $cart->model->attr_values }}"
                                                                       target="_blank"><img class="img-responsive"
                                                                                            src="{{ $cart->model->product->image }}"
                                                                                            alt="{{ $cart->model->product->title }} - {{ $cart->model->attr_values }}"></a>
                                                                </p></div>
                                                            <div class="col-right col-xs-9">
                                                                <div class="box-info-product"><p class="name"><a
                                                                            href="{{ $cart->model->attr_url }}"
                                                                            title="{{ $cart->model->product->title }} - {{ $cart->model->attr_values }}"
                                                                            target="_blank">{{ $cart->model->product->title }} - {{ $cart->model->attr_values }}</a></p>
                                                                    <p class="c-brands">Thương hiệu: {{ $cart->model->product->trademark }}</p>
                                                                    <p class="seller-by hidden">M</p>
                                                                    <p class="action">
                                                                        <a href="javascript:;"
                                                                                         class="btn btn-link btn-item-delete remove-item-cart"
                                                                                         data-id="{{ $cart->rowId }}" title="Xóa">Xóa</a>
                                                                    </p></div>
                                                                <div class="box-price"><p class="price pricechange" data-price="{{ $cart->price }}">{{ formatPrice($cart->price) }}</p></div>
                                                                <div class="quantity-block">
                                                                    <div class="input-group bootstrap-touchspin">
                                                                        <div class="input-group-btn"><input
                                                                                class="variantID" type="hidden"
                                                                                name="variantId" value="{{ $cart->rowId }}">
                                                                            <button
                                                                                onclick="var result = document.getElementById('qtyItem{{ $cart->rowId }}'); var qtyItem{{ $cart->rowId }} = result.value; if( !isNaN( qtyItem{{ $cart->rowId }} )) result.value++;return false;"
                                                                                class="increase_pop items-count btn-plus btn btn-default bootstrap-touchspin-up"
                                                                                type="button">+
                                                                            </button>
                                                                            <input type="text"
                                                                                   onchange="if(this.value == 0)this.value=1;"
                                                                                   maxlength="12" min="1"
                                                                                   class="form-control quantity-r2 quantity js-quantity-product input-text number-sidebar input_pop input_pop qtyItem{{ $cart->rowId }}"
                                                                                   id="qtyItem{{ $cart->rowId }}" name="Lines"
                                                                                   size="4" value="{{ $cart->qty }}">
                                                                            <button
                                                                                onclick="var result = document.getElementById('qtyItem{{ $cart->rowId }}'); var qtyItem{{ $cart->rowId }} = result.value; if( !isNaN( qtyItem{{ $cart->rowId }} ) &amp;&amp; qtyItem{{ $cart->rowId }} > 1 ) result.value--;return false;"
                                                                                class="reduced_pop items-count btn-minus btn btn-default bootstrap-touchspin-down"
                                                                                type="button">–
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <div class="col-xs-3 cart-col-2 cart-collaterals cart_submit">
                                                    <div id="right-affix">
                                                        <div class="each-row">
                                                            <div class="box-style fee"><p class="list-info-price"><span>Tạm tính: </span><strong
                                                                        class="totals_price price _text-right text_color_right1">{{ formatPrice(Cart::total(0, '', '')) }}</strong>
                                                                </p></div>
                                                            <div class="box-style fee">
                                                                <div class="total2 clearfix"><span class="text-label">Thành tiền: </span>
                                                                    <div class="amount"><p><strong class="totals_price">{{ formatPrice(Cart::total(0, '', '')) }}</strong>
                                                                        </p></div>
                                                                </div>
                                                            </div>
                                                            <button
                                                                class="button btn btn-large btn-block btn-danger btn-checkout evo-button"
                                                                title="Thanh toán ngay" type="button"
                                                                onclick="window.location.href='{{ route('checkout.index') }}'">Thanh toán
                                                                ngay
                                                            </button>
                                                            <button
                                                                class="button btn-proceed-checkout btn btn-large btn-block btn-danger btn-checkouts"
                                                                title="Tiếp tục mua hàng" type="button"
                                                                onclick="window.location.href='{{ route('product.index') }}'">Tiếp
                                                                tục mua hàng
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    @else
                                        <div class="col-md-6 col-md-offset-3">
                                            <div class="cart-empty">
                                                <span class="empty-icon"><i class="ico ico-cart"></i></span>
                                                <div class="btn-cart-empty">
                                                    <a class="btn btn-default" href="/" title="Tiếp tục mua sắm">Tiếp tục mua sắm</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="visible-sm visible-xs">
                            @if(Cart::count() > 0)
                            <div class="cart-mobile">
                                <form action="{{ route('cart.index') }}" method="post" class="margin-bottom-0">
                                    <div class="header-cart">
                                        <div class="title-cart clearfix">
                                            <h3>Giỏ hàng của bạn</h3>
                                        </div>
                                    </div>
                                    <div class="header-cart-content">
                                        <div class="cart_page_mobile content-product-list">
                                            @foreach(Cart::content() as $cart)
                                            <div class="item-product item productid-{{ $cart->rowId }}">
                                                <div class="item-product-cart-mobile">
                                                    <a class="product-images1" href="{{ $cart->model->attr_url }}"
                                                       title="{{ $cart->model->product->title }}">
                                                        <img width="80" height="150"
                                                             src="{{ $cart->model->product->image }}"
                                                             alt="{{ $cart->model->product->title }} - {{ $cart->model->attr_values }}"/>
                                                    </a>
                                                </div>
                                                <div class="title-product-cart-mobile">
                                                    <h3><a href="{{ $cart->model->attr_url }}"
                                                           title="{{ $cart->model->product->title }}">{{ $cart->model->product->title }}</a></h3>
                                                    <p>Giá: <span>{{ formatPrice($cart->price) }}</span></p>
                                                </div>
                                                <div class="select-item-qty-mobile">
                                                    <div class="txt_center">
                                                        <input class="variantID" type="hidden" name="variantId"
                                                               value="{{ $cart->rowId }}">
                                                        <button
                                                            onclick="var result = document.getElementById('qtyMobile{{ $cart->rowId }}'); var qtyMobile{{ $cart->rowId }} = result.value; if( !isNaN( qtyMobile{{ $cart->rowId }} ) &amp;&amp; qtyMobile{{ $cart->rowId }} > 0 ) result.value--;return false;"
                                                            class="reduced items-count btn-minus" type="button">–
                                                        </button>
                                                        <input type="text" maxlength="12" min="0"
                                                               class="input-text number-sidebar qtyMobile{{ $cart->rowId }}"
                                                               id="qtyMobile{{ $cart->rowId }}" name="Lines" size="4" value="{{ $cart->qty }}">
                                                        <button
                                                            onclick="var result = document.getElementById('qtyMobile{{ $cart->rowId }}'); var qtyMobile{{ $cart->rowId }} = result.value; if( !isNaN( qtyMobile{{ $cart->rowId }} )) result.value++;return false;"
                                                            class="increase items-count btn-plus" type="button">+
                                                        </button>
                                                    </div>
                                                    <a class="button remove-item remove-item-cart"
                                                       href="javascript:;"
                                                       data-id="{{ $cart->rowId }}">Xoá</a>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="header-cart-price">
                                            <div class="title-cart ">
                                                <h3 class="text-xs-left">Tổng tiền</h3>
                                                <a class="text-xs-right totals_price_mobile"> {{ formatPrice(Cart::total(0, '', '')) }}</a>
                                            </div>
                                            <div class="checkout">
                                                <button class="btn-proceed-checkout-mobile" title="Thanh toán ngay"
                                                        type="button" onclick="window.location.href='{{ route('checkout.index') }}'">
                                                    Thanh toán ngay
                                                </button>
                                                <button class="btn btn-primary btn-proceed-continues-mobile"
                                                        title="Tiếp tục mua hàng" type="button"
                                                        onclick="window.location.href='{{ route('product.index') }}'">
                                                    Tiếp tục mua hàng
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            @else
                                <div class="cart-empty">
                                    <span class="empty-icon"><i class="ico ico-cart"></i></span>
                                    <div class="btn-cart-empty">
                                        <a class="btn btn-default" href="/" title="Tiếp tục mua sắm">Tiếp tục mua hàng</a>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
