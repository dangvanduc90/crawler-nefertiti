
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title', config('app.name'))</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Chất liệu thiết kế Cao Cấp, Váy thiết kế Kiểu dáng Sang Trọng Nhất phù hợp đi làm, đi dự tiệc hay dạ hội đẹp ngây ngất">
    <meta name="keywords" content="Tất cả sản phẩm, @yield('title', config('app.name')), {{ url()->current() }}"/>
    <link rel="canonical" href="{{ url()->current() }}"/>
    <link rel="dns-prefetch" href="{{ url('/') }}">
    <link rel="dns-prefetch" href="//www.google-analytics.com/">
    <link rel="dns-prefetch" href="//www.googletagmanager.com/">
    <meta name='revisit-after' content='1 days' />
    <meta name="robots" content="noodp,index,follow" />
    <meta name="theme-color" content="#ff0080" />

    <link rel="icon" href="{{ setting('favicon') }}" type="image/x-icon" />
    <link rel="apple-touch-icon" href="{{ setting('favicon') }}">

    <meta property="og:type" content="website">
    <meta property="og:title" content="@yield('title', config('app.name'))">
    <meta property="og:image" content="{{ setting('logo') }}">
    <meta property="og:image:secure_url" content="{{ setting('logo') }}">
    <meta property="og:description" content="">
    <meta property="og:url" content="{{ url()->current() }}">
    <meta property="og:site_name" content="{{ config('app.name') }}">

    <link href='{{ asset('/css/bootstrap.scss.css') }}' rel='stylesheet'>
    <link href='{{ asset('/css/evo-index.scss.css') }}' rel='stylesheet'>
    <link href='{{ asset('/css/evo-main.scss.css') }}' rel='stylesheet'>
    <link href='{{ asset('/css/evo-collections.scss.css') }}' rel='stylesheet'>
    <link href='{{ asset('/css/style_update.scss.css') }}' rel='stylesheet'>
    <link href='{{ asset('/css/tkn-style.css') }}' rel='stylesheet'>
    <link href='{{ asset('/css/font-awesome-v5.7.2.css') }}' rel='stylesheet'>
    <script src="{{ asset('/js/jquery.js') }}" type="text/javascript"></script>
    @yield('head')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-2XL14JN9HL"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-2XL14JN9HL');
    </script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TQJPQ7J');</script>
    <!-- End Google Tag Manager -->
    <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "Organization",
      "name": "{{ config('app.name') }}",
      "logo": "{{ setting('logo') }}",
      "url": "{{ url()->current() }}",
      "contactPoint": {
        "@type": "ContactPoint",
        "telephone": "{{ setting('phone') }}",
        "email": "{{ setting('email') }}",
        "contactType":"customer support",
        "areaServed":"VN",
        "availableLanguage":"VN"
      },
      "sameAs": "{{ setting('facebook') }}"
    }
    </script>
    @yield('schema')
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQJPQ7J"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @include('layouts.top')
    @yield('body')
    @include('layouts.foot')
    @yield('foot')
    <script src="{{ asset('/js/evo-index-js.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/js/flipclock.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/js/main.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/js/foot.js') }}" type="text/javascript"></script>
</body>
</html>
