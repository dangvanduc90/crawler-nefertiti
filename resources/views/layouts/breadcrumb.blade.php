@if (count($breadcrumbs))
    <section class="bread-crumb margin-bottom-10">
        <div class="container">
            <ul class="breadcrumb" itemscope="" itemtype="https://schema.org/BreadcrumbList">
                @foreach ($breadcrumbs as $key => $breadcrumb)
                    @if ($breadcrumb->url && !$loop->last)
                        <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
                            <a itemprop="item" href="{{ $breadcrumb->url }}" title="{{ $breadcrumb->title }}">
                                <span itemprop="name">{{ $breadcrumb->title }}</span>
                                <meta itemprop="position" content="{{ $loop->iteration }}">
                            </a>
                            <span><i class="fa fa-angle-right"></i></span>
                        </li>
                    @else
                        <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
                            <strong itemprop="name">{{ $breadcrumb->title }}</strong>
                            <meta itemprop="position" content="{{ $loop->iteration }}">
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    </section>
@endif

