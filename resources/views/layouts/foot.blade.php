<footer class="footer bg-footer">
    <div class="wrapper-home-service" data-aos="fade-up">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="service-box clearfix">
                        <div class="icon">
                            <img class="lazy loaded"
                                 src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/store_policy_1.png?1624697016975"
                                 data-src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/store_policy_1.png?1624697016975"
                                 alt="Miễn phí giao hàng" data-was-processed="true">
                        </div>
                        <div class="detail-sv">
                            <h3 class="title"><a href="#" title="Miễn phí giao hàng">Miễn phí giao hàng</a></h3>
                            <p class="desc">Với hóa đơn từ 1 triệu VNĐ</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="service-box clearfix">
                        <div class="icon">
                            <img class="lazy loaded"
                                 src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/store_policy_2.png?1624697016975"
                                 data-src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/store_policy_2.png?1624697016975"
                                 alt="3 ngày đổi sản phẩm" data-was-processed="true">
                        </div>
                        <div class="detail-sv">
                            <h3 class="title"><a href="#" title="3 ngày đổi sản phẩm">3 ngày đổi sản phẩm</a></h3>
                            <p class="desc">Đổi sản phẩm trong vòng 3 ngày</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="service-box clearfix">
                        <div class="icon">
                            <img class="lazy loaded"
                                 src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/store_policy_3.png?1624697016975"
                                 data-src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/store_policy_3.png?1624697016975"
                                 alt="Mua hàng (8h00 - 22h30, T2 - CN)" data-was-processed="true">
                        </div>
                        <div class="detail-sv">
                            <h3 class="title"><a href="#" title="Mua hàng (8h00 - 22h30, T2 - CN)">Mua hàng (8h00 -
                                    22h30, T2 - CN)</a></h3>
                            <p class="desc">Hotline Mua hàng 09.1234.0789</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="service-box clearfix">
                        <div class="icon">
                            <img class="lazy loaded"
                                 src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/store_policy_4.png?1624697016975"
                                 data-src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/store_policy_4.png?1624697016975"
                                 alt="Hệ thống Showroom" data-was-processed="true">
                        </div>
                        <div class="detail-sv">
                            <h3 class="title"><a href="#" title="Hệ thống Showroom">Hệ thống Showroom</a></h3>
                            <p class="desc">10 showroom trên toàn hệ thống</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site-footer">
        <div class="container">
            <div class="footer-inner padding-bottom-20 padding-top-10">
                <div class="row footer-show-more-group">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 fix-clear">
                        <div class="footer-widget">
                            <h3> {{ config('app.name') }} </h3>
                            <ul class="list_menu_update cf">
                                <li>
                                    <span class="icon_fl"><i class="fa fa-map-marker"></i></span>
                                    <span class="icon_fr"><span>VPGD: 98 Tây Sơn, Đống Đa, Hà Nội  </span></span>
                                </li>
                                <li>
                                    <span class="icon_fl"><i class="fa fa-phone"></i></span>
                                    <span class="icon_fr">
										<span>
											<a href="tel:0912340789"> 09.1234.0789 </a>
										</span>
									</span>
                                </li>
                                <li>
                                    <span class="icon_fl"><i class="fa fa-envelope"></i></span>
                                    <span class="icon_fr">
										<span>
											<a href="mailto:CSKH@childory.org">CSKH@childory.org</a>
										</span>
									</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2 fix-clear">
                        <div class="footer-widget">
                            <h3>Chính sách</h3>
                            <ul class="list-menu has-click">
                                <li><a href="/phi-van-chuyen" title="Phí vận chuyển" rel="nofollow">Phí vận chuyển</a>
                                </li>
                                <li><a href="/dat-hang" title="Đặt hàng" rel="nofollow">Đặt hàng</a></li>
                                <li><a href="/tra-hang" title="Trả hàng" rel="nofollow">Trả hàng</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2 fix-clear">
                        <div class="footer-widget form-mailchimp">
                            <h3>Dịch vụ khách hàng</h3>
                            <ul class="list-menu has-click">
                                <li><a href="/lien-he" title="Liên hệ" rel="nofollow">Liên hệ</a></li>
                                <li><a href="/phuong-thuc-thanh-toan" title="Phương thức thanh toán" rel="nofollow">Phương
                                        thức thanh toán</a></li>
                                <li><a href="/diem-thuong" title="Điểm thưởng" rel="nofollow">Điểm thưởng</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 fix-clear">
                        <div class="footer-widget form-mailchimp">
                            <h3>Fanpage</h3>
                            <div id="fb-root" class=" fb_reset">
                                <div
                                    style="position: absolute; top: -10000px; width: 0; height: 0;">
                                    <div></div>
                                </div>
                            </div>
{{--                            <script>--}}
{{--                                (function (d, s, id) {--}}
{{--                                    var js, fjs = d.getElementsByTagName(s)[0];--}}
{{--                                    if (d.getElementById(id)) return;--}}
{{--                                    js = d.createElement(s);--}}
{{--                                    js.id = id;--}}
{{--                                    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=1557648114483134";--}}
{{--                                    fjs.parentNode.insertBefore(js, fjs);--}}
{{--                                }(document, 'script', 'facebook-jssdk'));--}}
{{--                            </script>--}}
{{--                            <div class="fb-page fb_iframe_widget" data-href="www.facebook.com/NefertitiFashion/"--}}
{{--                                 data-width="500" data-small-header="false" data-adapt-container-width="true"--}}
{{--                                 data-hide-cover="false" data-show-facepile="true" data-show-posts="false"--}}
{{--                                 fb-xfbml-state="rendered"--}}
{{--                                 fb-iframe-plugin-query="adapt_container_width=true&amp;app_id=1557648114483134&amp;container_width=370&amp;hide_cover=false&amp;href=www.facebook.com%2FNefertitiFashion%2F&amp;locale=en_US&amp;sdk=joey&amp;show_facepile=true&amp;show_posts=false&amp;small_header=false&amp;width=500">--}}
{{--                                <span style="vertical-align: bottom; width: 370px; height: 130px;"><iframe--}}
{{--                                        name="f39625bbb3af6bc" width="500px" height="1000px"--}}
{{--                                        data-testid="fb:page Facebook Social Plugin"--}}
{{--                                        title="fb:page Facebook Social Plugin" frameborder="0" allowtransparency="true"--}}
{{--                                        allowfullscreen="true" scrolling="no" allow="encrypted-media"--}}
{{--                                        src="https://www.facebook.com/v2.5/plugins/page.php?adapt_container_width=true&amp;app_id=1557648114483134&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df2e9266acc59c84%26domain%3Dnefertiti.com.vn%26origin%3Dhttps%253A%252F%252Fnefertiti.com.vn%252Ff349b07b5c00308%26relation%3Dparent.parent&amp;container_width=370&amp;hide_cover=false&amp;href=www.facebook.com%2FNefertitiFashion%2F&amp;locale=en_US&amp;sdk=joey&amp;show_facepile=true&amp;show_posts=false&amp;small_header=false&amp;width=500"--}}
{{--                                        style="border: none; visibility: visible; width: 370px; height: 130px;"--}}
{{--                                        class=""></iframe>--}}
{{--                                </span>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright clearfix">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <span>© Bản quyền thuộc về <b> {{ config('app.name') }} </b></span>
                </div>
            </div>
            <div class="back-to-top show" title="Lên đầu trang"><i class="fa fa-angle-up" aria-hidden="true"></i></div>
        </div>
    </div>
</footer>
