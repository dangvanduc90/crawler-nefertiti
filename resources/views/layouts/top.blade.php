<div class="evo-search-bar">
    <form action="{{ route('search') }}" method="get" class="has-validation-callback">
        <div class="input-group">
            <input type="text" name="query" class="search-auto form-control" placeholder="Thời trang công s"
                   aria-label="Tìm kiếm" autocomplete="off">
            <span class="input-group-btn">
				<button class="btn btn-default" type="submit" aria-label="Tìm kiếm"><i
                        class="fa fa-search"></i></button>
			</span>
        </div>
    </form>
    <button class="site-header__search" role="button" title="Đóng tìm kiếm">
        <svg xmlns="http://www.w3.org/2000/svg" width="26.045" height="26.044">
            <g data-name="Group 470">
                <path
                    d="M19.736 17.918l-4.896-4.896 4.896-4.896a1.242 1.242 0 0 0-.202-1.616 1.242 1.242 0 0 0-1.615-.202l-4.896 4.896L8.127 6.31a1.242 1.242 0 0 0-1.615.202 1.242 1.242 0 0 0-.202 1.615l4.895 4.896-4.896 4.896a1.242 1.242 0 0 0 .202 1.615 1.242 1.242 0 0 0 1.616.202l4.896-4.896 4.896 4.896a1.242 1.242 0 0 0 1.615-.202 1.242 1.242 0 0 0 .202-1.615z"
                    data-name="Path 224" fill="#1c1c1c"></path>
            </g>
        </svg>
    </button>
</div>
<header class="header">
    <div class="evo-header-logo-search-cart">
        <div class="container">
            <div class="row">
                <div class="col-md-4 hidden-sm hidden-xs">
                    <div class="evo-header-policy">
                        <ul class="free-ship j-free-shipping">
                            <li class="discount animated flipInX she-block"><a href="#"
                                                                               title=" Vận chuyển tiêu chuẩn miễn phí  cho đơn hàng trên  1.000.000₫ ">
                                    Vận chuyển tiêu chuẩn miễn phí cho đơn hàng trên <span>1.000.000₫</span></a></li>
                            <li class="discount she-none"><a href="#"
                                                             title="Giao hàng tiết kiệm cho đơn hàng trên  499.000₫ ">Giao
                                    hàng tiết kiệm cho đơn hàng trên <span>499.000₫</span></a></li>
                            <li class="discount she-none"><a href="#" title="3 ngày  đổi trả  dễ dàng">3 ngày <span>đổi trả</span>
                                    dễ dàng</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 logo evo-header-mobile">
                    <button type="button"
                            class="evo-flexitem evo-flexitem-fill navbar-toggle collapsed visible-sm visible-xs"
                            id="trigger-mobile" aria-label="Menu Mobile">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="/" class="logo-wrapper" title="{{ config('app.short_name') }}">
                        <img src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/logo.png?1624697016975"
                             data-src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/logo.png?1624697016975"
                             alt="{{ config('app.short_name') }}" class="lazy img-responsive center-block loaded"
                             data-was-processed="true">
                    </a>
                    <div class="evo-flexitem evo-flexitem-fill visible-sm visible-xs">
                        <a href="javascript:void(0);" class="site-header-search" rel="nofollow" title="Tìm kiếm"
                           aria-label="Tìm kiếm">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                        <a href="{{ route('cart.index') }}" title="Giỏ hàng" rel="nofollow">
                            <svg viewBox="0 0 100 100" data-radium="true" style="width: 25px;">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g transform="translate(-286.000000, -515.000000)" fill="#000">
                                        <path
                                            d="M374.302082,541.184324 C374.044039,539.461671 372.581799,538.255814 370.861517,538.255814 L351.078273,538.255814 L351.078273,530.159345 C351.078273,521.804479 344.283158,515 335.93979,515 C327.596422,515 320.801307,521.804479 320.801307,530.159345 L320.801307,538.255814 L301.018063,538.255814 C299.297781,538.255814 297.835541,539.461671 297.577499,541.184324 L286.051608,610.951766 C285.87958,611.985357 286.137623,613.018949 286.825735,613.794143 C287.513848,614.569337 288.460003,615 289.492173,615 L382.387408,615 L382.473422,615 C384.451746,615 386,613.449612 386,611.468562 C386,611.037898 385.913986,610.693368 385.827972,610.348837 L374.302082,541.184324 L374.302082,541.184324 Z M327.854464,530.159345 C327.854464,525.680448 331.467057,522.062877 335.93979,522.062877 C340.412524,522.062877 344.025116,525.680448 344.025116,530.159345 L344.025116,538.255814 L327.854464,538.255814 L327.854464,530.159345 L327.854464,530.159345 Z M293.62085,608.023256 L304.028557,545.318691 L320.801307,545.318691 L320.801307,565.043066 C320.801307,567.024117 322.349561,568.574505 324.327886,568.574505 C326.30621,568.574505 327.854464,567.024117 327.854464,565.043066 L327.854464,545.318691 L344.025116,545.318691 L344.025116,565.043066 C344.025116,567.024117 345.57337,568.574505 347.551694,568.574505 C349.530019,568.574505 351.078273,567.024117 351.078273,565.043066 L351.078273,545.318691 L367.851024,545.318691 L378.25873,608.023256 L293.62085,608.023256 L293.62085,608.023256 Z"></path>
                                    </g>
                                </g>
                            </svg>
                            <span class="count_item_pr">{{ Cart::count() }}</span>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 evo-header-hotline-cart hidden-sm hidden-xs">
                    <div class="hotline">
                        <div class="user-content-right">
                            <a href="tel:09.1234.0789" title="Hỗ trợ mua hàng 09.1234.0789">
                                <span class="evo-hotline">09.1234.0789</span>
                                <span class="evo-title">Hỗ trợ mua hàng</span>
                            </a>
                        </div>
                    </div>
                    <div class="evo-search">
                        <a href="javascript:void(0);" class="site-header-search" rel="nofollow" title="Tìm kiếm">
                            <svg viewBox="0 0 451 451" style="width:25px;">
                                <g fill="#000">
                                    <path d="M447.05,428l-109.6-109.6c29.4-33.8,47.2-77.9,47.2-126.1C384.65,86.2,298.35,0,192.35,0C86.25,0,0.05,86.3,0.05,192.3
								s86.3,192.3,192.3,192.3c48.2,0,92.3-17.8,126.1-47.2L428.05,447c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4
								C452.25,441.8,452.25,433.2,447.05,428z M26.95,192.3c0-91.2,74.2-165.3,165.3-165.3c91.2,0,165.3,74.2,165.3,165.3
								s-74.1,165.4-165.3,165.4C101.15,357.7,26.95,283.5,26.95,192.3z"></path>
                                </g>
                            </svg>
                        </a>
                    </div>
                    <div class="evo-account">
                        <a href="javascript:void(0);" title="Tài khoản" rel="nofollow">
                            <img src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/user.svg?1624697016975"
                                 alt="{{ config('app.short_name') }}" class="img-responsive">
                        </a>
                        <ul>
                            <li><a rel="nofollow" href="/account/login" title="Đăng nhập">Đăng nhập</a></li>
                            <li><a rel="nofollow" href="/account/register" title="Đăng ký">Đăng ký</a></li>
                        </ul>
                    </div>
                    <div class="evo-cart mini-cart">
                        <a href="{{ route('cart.index') }}" title="Giỏ hàng" rel="nofollow">
                            <svg viewBox="0 0 100 100" data-radium="true" style="width: 25px;">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g transform="translate(-286.000000, -515.000000)" fill="#000">
                                        <path
                                            d="M374.302082,541.184324 C374.044039,539.461671 372.581799,538.255814 370.861517,538.255814 L351.078273,538.255814 L351.078273,530.159345 C351.078273,521.804479 344.283158,515 335.93979,515 C327.596422,515 320.801307,521.804479 320.801307,530.159345 L320.801307,538.255814 L301.018063,538.255814 C299.297781,538.255814 297.835541,539.461671 297.577499,541.184324 L286.051608,610.951766 C285.87958,611.985357 286.137623,613.018949 286.825735,613.794143 C287.513848,614.569337 288.460003,615 289.492173,615 L382.387408,615 L382.473422,615 C384.451746,615 386,613.449612 386,611.468562 C386,611.037898 385.913986,610.693368 385.827972,610.348837 L374.302082,541.184324 L374.302082,541.184324 Z M327.854464,530.159345 C327.854464,525.680448 331.467057,522.062877 335.93979,522.062877 C340.412524,522.062877 344.025116,525.680448 344.025116,530.159345 L344.025116,538.255814 L327.854464,538.255814 L327.854464,530.159345 L327.854464,530.159345 Z M293.62085,608.023256 L304.028557,545.318691 L320.801307,545.318691 L320.801307,565.043066 C320.801307,567.024117 322.349561,568.574505 324.327886,568.574505 C326.30621,568.574505 327.854464,567.024117 327.854464,565.043066 L327.854464,545.318691 L344.025116,545.318691 L344.025116,565.043066 C344.025116,567.024117 345.57337,568.574505 347.551694,568.574505 C349.530019,568.574505 351.078273,567.024117 351.078273,565.043066 L351.078273,545.318691 L367.851024,545.318691 L378.25873,608.023256 L293.62085,608.023256 L293.62085,608.023256 Z"></path>
                                    </g>
                                </g>
                            </svg>
                            <span class="count_item_pr">{{ Cart::count() }}</span>
                        </a>
                        <div class="top-cart-content">
                            <ul id="cart-sidebar" class="mini-products-list count_li">
                                @if(Cart::total() > 0)
                                    <ul class="list-item-cart">
                                        @foreach(Cart::content() as $cart)
                                        <li class="item productid-{{ $cart->rowId }}"><a class="product-image" href="{{ $cart->model->product->slug }}"
                                                                               title="{{ $cart->model->product->title }} - {{ $cart->model->attr_values }}"><img
                                                    alt="{{ $cart->model->product->title }} - {{ $cart->model->attr_values }}"
                                                    src="{{ $cart->model->product->image }}"
                                                    width="80"></a>
                                            <div class="detail-item">
                                                <div class="product-details"><a href="javascript:;" data-id="{{ $cart->rowId }}"
                                                                                title="Xóa"
                                                                                class="remove-item-cart far fa-trash-alt">&nbsp;</a>
                                                    <p class="product-name"><a href="{{ $cart->model->product->slug }}"
                                                                               title="{{ $cart->model->product->title }} - {{ $cart->model->attr_values }}">{{ $cart->model->product->title }} - {{ $cart->model->attr_values }}</a></p></div>
                                                <div class="product-details-bottom"><span
                                                        class="price pricechange" data-price="{{ $cart->price }}">{{ formatPrice($cart->price) }}</span>
                                                    <div class="quantity-select"><input class="variantID" type="hidden"
                                                                                        name="variantId" value="{{ $cart->rowId }}">
                                                        <button
                                                            onclick="var result = document.getElementById('qty{{ $cart->rowId }}'); var qty{{ $cart->rowId }} = result.value; if( !isNaN( qty{{ $cart->rowId }} ) &amp;&amp; qty{{ $cart->rowId }} > 1 ) result.value--;return false;"
                                                            class="reduced items-count btn-minus" type="button">
                                                            –
                                                        </button>
                                                        <input type="text" maxlength="3" min="1"
                                                               onchange="if(this.value == 0)this.value=1;"
                                                               class="input-text number-sidebar qty{{ $cart->rowId }}"
                                                               id="qty{{ $cart->rowId }}" name="Lines" size="4" value="{{ $cart->qty }}">
                                                        <button
                                                            onclick="var result = document.getElementById('qty{{ $cart->rowId }}'); var qty{{ $cart->rowId }} = result.value; if( !isNaN( qty{{ $cart->rowId }} )) result.value++;return false;"
                                                            class="increase items-count btn-plus" type="button">+
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                    <div>
                                        <div class="top-subtotal">Tổng cộng: <span class="price">{{ formatPrice(Cart::total(0, '', '')) }}</span></div>
                                    </div>
                                    <div>
                                        <div class="actions clearfix"><a href="{{ route('checkout.index') }}" class="btn btn-gray btn-checkout"
                                                                         title="Thanh toán"><span>Thanh toán</span></a><a
                                                href="{{ route('cart.index') }}" class="view-cart btn btn-white margin-left-5" title="Giỏ hàng"><span>Giỏ hàng</span></a>
                                        </div>
                                    </div>
                                @else
                                    <div class="no-item"><p>Không có sản phẩm nào trong giỏ hàng.</p></div>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="evo-main-nav affix-top" data-spy="affix" data-offset="80">
        <div class="container">
            <div class="row">
                <div class="col-md-12 no-padding">
                    <div class="mobile-main-menu hidden-lg hidden-md">
                        <div class="drawer-header">
                            <div id="close-nav">
                                <svg viewBox="0 0 100 100" data-radium="true" style="width: 16px;">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g transform="translate(-645.000000, -879.000000)" fill="#000">
                                            <path
                                                d="M743.998989,926.504303 L697.512507,880.032702 C696.909905,879.430293 695.962958,879 695.016011,879 C694.069064,879 693.122117,879.430293 692.519515,880.032702 L646.033033,926.504303 C644.655656,927.881239 644.655656,930.118761 646.033033,931.495697 C646.721722,932.184165 647.582582,932.528399 648.529529,932.528399 C649.476476,932.528399 650.337337,932.184165 651.026025,931.495697 L691.486482,891.048193 L691.486482,975.471601 C691.486482,977.450947 693.036031,979 695.016011,979 C696.995991,979 698.54554,977.450947 698.54554,975.471601 L698.54554,891.048193 L739.005997,931.495697 C740.383374,932.872633 742.621612,932.872633 743.998989,931.495697 C745.376366,930.118761 745.29028,927.881239 743.998989,926.504303 L743.998989,926.504303 Z"
                                                transform="translate(695.000000, 929.000000) rotate(-90.000000) translate(-695.000000, -929.000000) "></path>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <a href="/" class="logo-wrapper" title="{{ config('app.short_name') }}">
                                <img
                                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"
                                    data-src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/logo.png?1624697016975"
                                    alt="{{ config('app.short_name') }}" class="lazy img-responsive center-block">
                            </a>
                        </div>
                        <div class="ul-first-menu">
                            <a rel="nofollow" href="/account/login" title="Đăng nhập">Đăng nhập</a>
                            <a rel="nofollow" href="/account/register" title="Đăng ký">Đăng ký</a>
                        </div>
                    </div>
                    <ul id="nav" class="nav">
                        <li class="nav-item active"><a class="nav-link" href="/" title="Trang chủ">Trang chủ</a></li>
                        <li class="nav-item "><a class="nav-link" href="/gioi-thieu" title="Giới thiệu">Giới thiệu</a>
                        </li>
                        <li class=" nav-item has-childs ">
                            <a href="{{ route('product.index') }}" class="nav-link" title="Thời trang nữ">Thời trang nữ <i
                                    class="fa fa-angle-down" data-toggle="dropdown"></i></a>
                            <ul class="dropdown-menu">
                                <li class="nav-item-lv2"><a class="nav-link" href="{{ route('category.show', 'san-pham-moi') }}"
                                                            title="Sản phẩm mới">Sản phẩm mới</a></li>
                                <li class="nav-item-lv2"><a class="nav-link" href="{{ route('category.show', 'dam') }}" title="Đầm">Đầm</a></li>
                                <li class="nav-item-lv2"><a class="nav-link" href="{{ route('category.show', 'ao-so-mi') }}" title="Áo sơ mi">Áo sơ
                                        mi</a></li>
                                <li class="nav-item-lv2"><a class="nav-link" href="{{ route('category.show', 'chan-vay-cong-so') }}" title="Chân váy công sở">Chân
                                        váy công sở</a></li>
                                <li class="nav-item-lv2"><a class="nav-link" href="{{ route('category.show', 'quan-au') }}" title="Quần âu">Quần âu</a>
                                </li>
                                <li class="nav-item-lv2"><a class="nav-link" href="{{ route('category.show', 'bo-cong-so') }}" title="Bộ công sở">Bộ
                                        công sở</a></li>
                                <li class="nav-item-lv2"><a class="nav-link" href="{{ route('category.show', 'mang-to') }}" title="Măng tô">Măng tô</a>
                                </li>
                                <li class="nav-item-lv2"><a class="nav-link" href="{{ route('category.show', 'vest-nu') }}" title="Vest nữ">Vest nữ</a>
                                </li>
                            </ul>
                        </li>
                        <li class=" nav-item has-childs ">
                            <a href="/thoi-trang-nam" class="nav-link" title="Thời trang nam">Thời trang nam <i
                                    class="fa fa-angle-down" data-toggle="dropdown"></i></a>
                            <ul class="dropdown-menu">
                                <li class="nav-item-lv2"><a class="nav-link" href="/thoi-trang-nam"
                                                            title="Sản phẩm mới">Sản phẩm mới</a></li>
                            </ul>
                        </li>
                        <li class=" nav-item has-childs ">
                            <a href="/sale-off" class="nav-link" title="SALE OFF">SALE OFF <i class="fa fa-angle-down"
                                                                                              data-toggle="dropdown"></i></a>
                            <ul class="dropdown-menu">
                                <li class="nav-item-lv2"><a class="nav-link" href="/dong-gia-699"
                                                            title="Đồng giá 699.000">Đồng giá 699.000</a></li>
                                <li class="nav-item-lv2"><a class="nav-link" href="/sale-off-50" title="Sale off 50%">Sale
                                        off 50%</a></li>
                                <li class="nav-item-lv2"><a class="nav-link" href="/dong-gia-499"
                                                            title="Đồng giá 499.000">Đồng giá 499.000</a></li>
                                <li class="nav-item-lv2"><a class="nav-link" href="/dong-gia-599"
                                                            title="Đồng giá 599.000">Đồng giá 599.000</a></li>
                                <li class="nav-item-lv2"><a class="nav-link" href="/dong-gia-399"
                                                            title="Đồng giá 399.000">Đồng giá 399.000</a></li>
                                <li class="nav-item-lv2"><a class="nav-link" href="/dong-gia-299"
                                                            title="Đồng giá 299.000">Đồng giá 299.000</a></li>
                                <li class="nav-item-lv2"><a class="nav-link" href="/dong-gia-199"
                                                            title="Đồng giá 199.000">Đồng giá 199.000</a></li>
                                <li class="nav-item-lv2"><a class="nav-link" href="/dong-gia-99"
                                                            title="Đồng giá 99.000">Đồng giá 99.000</a></li>
                            </ul>
                        </li>
                        <li class="nav-item "><a class="nav-link" href="/tin-tuc" title="Tin tức">Tin tức</a></li>
                        <li class="nav-item "><a class="nav-link" href="/tin-tuc/chinh-sach-khach-hang"
                                                 title="Chính sách khách hàng">Chính sách khách hàng</a></li>
                        <li class="nav-item "><a class="nav-link" href="/showroom" title="Hệ thống cửa hàng">Hệ thống
                                cửa hàng</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="affix_line"></div>
</header>
