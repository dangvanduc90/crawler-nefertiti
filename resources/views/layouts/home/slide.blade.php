<section class="awe-section-1">
    <div class="home-slider">
        <div class="item">
            <a href="#" class="clearfix" title="">
                <picture>
                    <source
                        media="(min-width: 1200px)"
                        srcset="https://bizweb.dktcdn.net//100/404/112/themes/787627/assets/slider_1.jpg?1624697016975">
                    <source
                        media="(min-width: 992px)"
                        srcset="https://bizweb.dktcdn.net//100/404/112/themes/787627/assets/slider_1.jpg?1624697016975">
                    <source
                        media="(min-width: 569px)"
                        srcset="https://bizweb.dktcdn.net//100/404/112/themes/787627/assets/slider_1.jpg?1624697016975">
                    <source
                        media="(min-width: 480px)"
                        srcset="https://bizweb.dktcdn.net//thumb/large/100/404/112/themes/787627/assets/slider_1.jpg?1624697016975">
                    <img
                        src="https://bizweb.dktcdn.net//thumb/grande/100/404/112/themes/787627/assets/slider_1.jpg?1624697016975"
                        alt="" class="lazy img-responsive center-block"/>
                </picture>
            </a>
        </div>
        <div class="item">
            <a href="#" class="clearfix" title="">
                <picture>
                    <source
                        media="(min-width: 1200px)"
                        srcset="https://bizweb.dktcdn.net//100/404/112/themes/787627/assets/slider_2.jpg?1624697016975">
                    <source
                        media="(min-width: 992px)"
                        srcset="https://bizweb.dktcdn.net//100/404/112/themes/787627/assets/slider_2.jpg?1624697016975">
                    <source
                        media="(min-width: 569px)"
                        srcset="https://bizweb.dktcdn.net//100/404/112/themes/787627/assets/slider_2.jpg?1624697016975">
                    <source
                        media="(min-width: 480px)"
                        srcset="https://bizweb.dktcdn.net//thumb/large/100/404/112/themes/787627/assets/slider_2.jpg?1624697016975">
                    <img
                        src="https://bizweb.dktcdn.net//thumb/grande/100/404/112/themes/787627/assets/slider_2.jpg?1624697016975"
                        alt="" class="lazy img-responsive center-block"/>
                </picture>
            </a>
        </div>
        <div class="item">
            <a href="#" class="clearfix" title="">
                <picture>
                    <source
                        media="(min-width: 1200px)"
                        srcset="https://bizweb.dktcdn.net//100/404/112/themes/787627/assets/slider_3.jpg?1624697016975">
                    <source
                        media="(min-width: 992px)"
                        srcset="https://bizweb.dktcdn.net//100/404/112/themes/787627/assets/slider_3.jpg?1624697016975">
                    <source
                        media="(min-width: 569px)"
                        srcset="https://bizweb.dktcdn.net//100/404/112/themes/787627/assets/slider_3.jpg?1624697016975">
                    <source
                        media="(min-width: 480px)"
                        srcset="https://bizweb.dktcdn.net//thumb/large/100/404/112/themes/787627/assets/slider_3.jpg?1624697016975">
                    <img
                        src="https://bizweb.dktcdn.net//thumb/grande/100/404/112/themes/787627/assets/slider_3.jpg?1624697016975"
                        alt="" class="lazy img-responsive center-block"/>
                </picture>
            </a>
        </div>
    </div>
</section>
