<section class="awe-section-10">
    <div class="container section_blogs">
        <div class="new_title">
            <h3><a href="tin-tuc" title="Tin thời trang">Tin thời trang</a></h3>
        </div>
        <div class="evo-owl-blog evo-slick">
            <div class="news-items">
                <a href="/chao-thang-phu-nu-voi-sieu-uu-dai-len-toi-50-toan-bo-san-pham"
                   title="Chào tháng ph&#7909; n&#7919; v&#7899;i siêu ưu đãi lên t&#7899;i 50% toàn b&#7897; s&#7843;n ph&#7849;m"
                   class="clearfix evo-item-blogs">
                    <div class="evo-article-image">
                        <img
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"
                            data-src="//bizweb.dktcdn.net/thumb/large/100/404/112/articles/z2355337555081-d17b8ef52401be24c6527964b652ccf6.jpg?v=1614669527530"
                            alt="Chào tháng ph&#7909; n&#7919; v&#7899;i siêu ưu đãi lên t&#7899;i 50% toàn b&#7897; s&#7843;n ph&#7849;m"
                            class="lazy img-responsive center-block"/>
                    </div>
                    <h3 class="line-clamp">Chào tháng ph&#7909; n&#7919; v&#7899;i siêu ưu đãi lên t&#7899;i 50% toàn b&#7897;
                        s&#7843;n ph&#7849;m</h3>
                    <p>
                        𝑻𝒉𝒂́𝒏𝒈 3 - 𝒕𝒉𝒂́𝒏𝒈 𝒚𝒆̂𝒖 𝒕𝒉𝒖̛𝒐̛𝒏𝒈, chúc phái đẹp luôn thành công trong cuộc
                        sống, luô...</p>
                </a>
            </div>
            <div class="news-items">
                <a href="/sale-sap-san-dong-gia-chi-tu-199k-tai-nefertiti-fashion"
                   title="SALE S&#7852;P SÀN - Đ&#7890;NG GIÁ CH&#7880; T&#7914; 199K t&#7841;i {{ config('app.name') }}"
                   class="clearfix evo-item-blogs">
                    <div class="evo-article-image">
                        <img
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"
                            data-src="//bizweb.dktcdn.net/thumb/large/100/404/112/articles/z2296201245740-aa2cb0aacc5edd8163e5ff870f45710e.jpg?v=1611565617590"
                            alt="SALE S&#7852;P SÀN - Đ&#7890;NG GIÁ CH&#7880; T&#7914; 199K t&#7841;i {{ config('app.name') }}"
                            class="lazy img-responsive center-block"/>
                    </div>
                    <h3 class="line-clamp">SALE S&#7852;P SÀN - Đ&#7890;NG GIÁ CH&#7880; T&#7914; 199K t&#7841;i
                        {{ config('app.name') }}</h3>
                    <p> Ưu đãi cuối cùng
                        Tưng bừng giảm giá
                        Chào năm mới – Sale sớm tối
                        Tết đến - Sale tới bến, {{ config('app.short_name') }} áp d...</p>
                </a>
            </div>
            <div class="news-items">
                <a href="/sale-het-don-tet-tha-ga-mua-sam-voi-uu-dai-len-den-50-tai-nefertiti"
                   title="SALE H&#7870;T - ĐÓN T&#7870;T, th&#7843; ga mua s&#7855;m v&#7899;i ưu đãi lên đ&#7871;n 50% t&#7841;i {{ config('app.short_name') }}"
                   class="clearfix evo-item-blogs">
                    <div class="evo-article-image">
                        <img
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"
                            data-src="//bizweb.dktcdn.net/thumb/large/100/404/112/articles/post.jpg?v=1610359422013"
                            alt="SALE H&#7870;T - ĐÓN T&#7870;T, th&#7843; ga mua s&#7855;m v&#7899;i ưu đãi lên đ&#7871;n 50% t&#7841;i {{ config('app.short_name') }}"
                            class="lazy img-responsive center-block"/>
                    </div>
                    <h3 class="line-clamp">SALE H&#7870;T - ĐÓN T&#7870;T, th&#7843; ga mua s&#7855;m v&#7899;i ưu đãi
                        lên đ&#7871;n 50% t&#7841;i {{ config('app.short_name') }}</h3>
                    <p> Từ ngày 12/01/2021 đến 10/02/2021, thương hiệu thời trang {{ config('app.short_name') }} giảm giá sốc lên đến 50%
                        toàn bộ các ...</p>
                </a>
            </div>
            <div class="news-items">
                <a href="/don-giang-sinh-lung-linh-qua-tang-cung-nefertiti-fashion"
                   title="Đón Giáng Sinh lung linh quà t&#7863;ng cùng {{ config('app.name') }}"
                   class="clearfix evo-item-blogs">
                    <div class="evo-article-image">
                        <img
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"
                            data-src="//bizweb.dktcdn.net/thumb/large/100/404/112/articles/z2234219626872-d703b17144b83439f5722940a8e12293.jpg?v=1608284925413"
                            alt="Đón Giáng Sinh lung linh quà t&#7863;ng cùng {{ config('app.name') }}"
                            class="lazy img-responsive center-block"/>
                    </div>
                    <h3 class="line-clamp">Đón Giáng Sinh lung linh quà t&#7863;ng cùng {{ config('app.name') }}</h3>
                    <p>
                        Những ngày này, không khí Giáng sinh đã ngập tràn, tiếng nhạc rộn ràng hòa dưới những ánh đèn
                        lung lin...</p>
                </a>
            </div>
        </div>
    </div>
</section>
