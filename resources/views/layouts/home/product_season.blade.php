<section class="awe-section-6">
    <div class="section_san_pham" data-aos="fade-up">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="e-tabs not-dqtab ajax-tab-2" data-section="ajax-tab-2" data-view="grid_4">
                        <div class="content">
                            <div class="titlecp clearfix">
                                <h3><a href="{{ route('product.index') }}" title="Autumn Winter 2020">Autumn Winter 2020</a></h3>
                                <span class="hidden-md hidden-lg button_show_tab">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</span>
                                <ul class="tabs tabs-title tab-desktop ajax clearfix evo-close">
                                    <li class="tab-link has-content" data-tab="tab-1" data-url="/san-pham-moi">
                                        <span title="Sản phẩm mới">Sản phẩm mới</span>
                                    </li>
                                    <li class="tab-link " data-tab="tab-2" data-url="{{ route('category.show', 'dam') }}">
                                        <span title="Đầm">Đầm</span>
                                    </li>
                                    <li class="tab-link " data-tab="tab-3" data-url="{{ route('category.show', 'ao-so-mi') }}">
                                        <span title="Áo sơ mi">Áo sơ mi</span>
                                    </li>
                                    <li class="tab-link " data-tab="tab-4" data-url="{{ route('category.show', 'chan-vay-cong-so') }}">
                                        <span title="Chân váy">Chân váy</span>
                                    </li>
                                    <li class="tab-link " data-tab="tab-5" data-url="{{ route('category.show', 'quan-au') }}">
                                        <span title="Quần âu">Quần âu</span>
                                    </li>
                                    <li class="tab-link " data-tab="tab-6" data-url="{{ route('category.show', 'quan-thun') }}">
                                        <span title="Quần thun">Quần thun</span>
                                    </li>
                                    <li class="tab-link " data-tab="tab-7" data-url="{{ route('category.show', 'bo-cong-so') }}">
                                        <span title="Bộ công sở">Bộ công sở</span>
                                    </li>
                                    <li class="tab-link " data-tab="tab-8" data-url="{{ route('category.show', 'vest-nu') }}">
                                        <span title="Vest nữ">Vest nữ</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-1 tab-content">
                                <div class="slick_ajax_tab products-view-grid">
                                    @foreach($productBySeason as $product)
                                    <div class="item">
                                        <div class="evo-product-block-item">
                                            <div class="product_thumb">
                                                <a class="primary_img" href="{{ route('product.show', $product->slug) }}">
                                                    <img class="lazy"
                                                         src="{{ $product->image }}"
                                                         data-src="{{ $product->image }}"
                                                         alt="{{ $product->title }}"/>
                                                </a>
                                                <a class="secondary_img" href="{{ route('product.show', $product->slug) }}"
                                                   title="{{ $product->title }}">
                                                    <img class="lazy"
                                                         src="{{ $product->image }}"
                                                         data-src="{{ $product->image }}"
                                                         alt="{{ $product->title }}"/>
                                                </a>
                                                <form action="{{ route('cart.store') }}" method="post" enctype="multipart/form-data"
                                                      class="cart-wrap hidden-sm hidden-xs hidden-md variants form-nut-grid form-ajaxtocart"
                                                      data-id="product-actions-{{ $product->id }}">
                                                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                                                    <input type="hidden" name="product_title" value="{{ $product->title }}">
                                                    <input type="hidden" name="product_price" value="{{ $product->price }}">
                                                    <button data-toggle="tooltip" data-placement="top" title="Thêm vào giỏ hàng"
                                                            type="button" onclick="location.href='{{ route('product.show', $product->slug) }}'"
                                                            class="action cart-button option-icons fas"></button>
                                                </form>
                                            </div>
                                            <div class="product_content">
                                                <div class="product_name">
                                                    <h4><a href="{{ route('product.show', $product->slug) }}" title="{{ $product->title }}">{{ $product->title }}</a></h4>
                                                </div>
                                                <div class="price-container">
                                                    <span class="current_price">{{ formatPrice($product->price) }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="tab-2 tab-content">
                            </div>
                            <div class="tab-3 tab-content">
                            </div>
                            <div class="tab-4 tab-content">
                            </div>
                            <div class="tab-5 tab-content">
                            </div>
                            <div class="tab-6 tab-content">
                            </div>
                            <div class="tab-7 tab-content">
                            </div>
                            <div class="tab-8 tab-content">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
