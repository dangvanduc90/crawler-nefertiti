<section class="awe-section-2">
    <link rel="preload" as="script" href="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/flipclock.js?1624697016975">
    <script src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/flipclock.js?1624697016975" type="text/javascript"></script>
    <div class="section_gift" data-aos="zoom-in">
        <div class="container">
            <div class="titlecp clearfix">
                <h3><a href="sale-off-50" title="SUMMER SALE"><span class="sale-flash-icon"><i class="fab fa-gripfire"></i></span>SUMMER SALE</a></h3>


                <div class="timer">
                    <h5>Kết thúc sau</h5>
                    <div class="timer-view">
                        <div class="clock flip-clock-wrapper"><span class="flip-clock-divider days"><span class="flip-clock-label">Days</span></span><ul class="flip "><li class="flip-clock-before"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">-</div></div><div class="down"><div class="shadow"></div><div class="inn">-</div></div></a></li><li class="flip-clock-active"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">-</div></div><div class="down"><div class="shadow"></div><div class="inn">-</div></div></a></li></ul><ul class="flip "><li class="flip-clock-before"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">8</div></div><div class="down"><div class="shadow"></div><div class="inn">8</div></div></a></li><li class="flip-clock-active"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">9</div></div><div class="down"><div class="shadow"></div><div class="inn">9</div></div></a></li></ul><ul class="flip "><li class="flip-clock-before"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">1</div></div><div class="down"><div class="shadow"></div><div class="inn">1</div></div></a></li><li class="flip-clock-active"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">1</div></div><div class="down"><div class="shadow"></div><div class="inn">1</div></div></a></li></ul><ul class="flip "><li class="flip-clock-before"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">-</div></div><div class="down"><div class="shadow"></div><div class="inn">-</div></div></a></li><li class="flip-clock-active"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">-</div></div><div class="down"><div class="shadow"></div><div class="inn">-</div></div></a></li></ul><ul class="flip "><li class="flip-clock-before"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">1</div></div><div class="down"><div class="shadow"></div><div class="inn">1</div></div></a></li><li class="flip-clock-active"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">1</div></div><div class="down"><div class="shadow"></div><div class="inn">1</div></div></a></li></ul><span class="flip-clock-divider hours"><span class="flip-clock-label">Hours</span><span class="flip-clock-dot top"></span><span class="flip-clock-dot bottom"></span></span><ul class="flip "><li class="flip-clock-before"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">-</div></div><div class="down"><div class="shadow"></div><div class="inn">-</div></div></a></li><li class="flip-clock-active"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">-</div></div><div class="down"><div class="shadow"></div><div class="inn">-</div></div></a></li></ul><ul class="flip "><li class="flip-clock-before"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">2</div></div><div class="down"><div class="shadow"></div><div class="inn">2</div></div></a></li><li class="flip-clock-active"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">3</div></div><div class="down"><div class="shadow"></div><div class="inn">3</div></div></a></li></ul><span class="flip-clock-divider minutes"><span class="flip-clock-label">Minutes</span><span class="flip-clock-dot top"></span><span class="flip-clock-dot bottom"></span></span><ul class="flip "><li class="flip-clock-before"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">4</div></div><div class="down"><div class="shadow"></div><div class="inn">4</div></div></a></li><li class="flip-clock-active"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">5</div></div><div class="down"><div class="shadow"></div><div class="inn">5</div></div></a></li></ul><ul class="flip "><li class="flip-clock-before"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">-</div></div><div class="down"><div class="shadow"></div><div class="inn">-</div></div></a></li><li class="flip-clock-active"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">-</div></div><div class="down"><div class="shadow"></div><div class="inn">-</div></div></a></li></ul><span class="flip-clock-divider seconds"><span class="flip-clock-label">Seconds</span><span class="flip-clock-dot top"></span><span class="flip-clock-dot bottom"></span></span><ul class="flip "><li class="flip-clock-before"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">4</div></div><div class="down"><div class="shadow"></div><div class="inn">4</div></div></a></li><li class="flip-clock-active"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">5</div></div><div class="down"><div class="shadow"></div><div class="inn">5</div></div></a></li></ul><ul class="flip "><li class="flip-clock-before"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">1</div></div><div class="down"><div class="shadow"></div><div class="inn">1</div></div></a></li><li class="flip-clock-active"><a href="#"><div class="up"><div class="shadow"></div><div class="inn">1</div></div><div class="down"><div class="shadow"></div><div class="inn">1</div></div></a></li></ul></div>
                    </div>
                </div>

            </div>
            <div class="evo-index-product-contain">

                <div class="alert alert-warning alert-dismissible fade in" role="alert">
                    <strong>SUMMER SALE!</strong> Đang được cập nhật nội dung.
                </div>

            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            var today = new Date();
            today = today.getTime()/1000;
            var finalDate = new Date();
            finalDate.setFullYear(2021);
            finalDate.setMonth(4 - 1);
            finalDate.setDate(30);
            finalDate.setHours(23);
            finalDate.setMinutes(59);
            finalDate.setSeconds(59);
            finalDate  = finalDate.getTime()/1000;
            var diff  = finalDate - today;
            $('.clock').FlipClock(diff, {
                clockFace: 'DailyCounter',
                countdown: true,
                callbacks: {
                    interval: function() {
                        var time = this.factory.getTime().time;

                        if(time) {
                            console.log('interval', time);
                        }
                    }
                }
            });
        });
    </script>
</section>
