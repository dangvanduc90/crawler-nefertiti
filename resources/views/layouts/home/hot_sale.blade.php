<section class="awe-section-5">
    <div class="section_san_pham" data-aos="fade-up">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="e-tabs not-dqtab ajax-tab-1" data-section="ajax-tab-1" data-view="grid_4">
                        <div class="content">
                            <div class="titlecp clearfix">
                                <h3><a href="/collections/all" title="HOT SALE">HOT SALE</a></h3>
                                <span class="hidden-md hidden-lg button_show_tab">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</span>
                                <ul class="tabs tabs-title tab-desktop ajax clearfix evo-close">
                                    <li class="tab-link has-content" data-tab="tab-1" data-url="/dong-gia-699">
                                        <span title="Đồng giá 699.000">Đồng giá 699.000</span>
                                    </li>
                                    <li class="tab-link " data-tab="tab-2" data-url="/sale-off-50">
                                        <span title="Sale off 50%">Sale off 50%</span>
                                    </li>
                                    <li class="tab-link " data-tab="tab-3" data-url="/dong-gia-499">
                                        <span title="Đồng giá 499.000">Đồng giá 499.000</span>
                                    </li>
                                    <li class="tab-link " data-tab="tab-4" data-url="/dong-gia-599">
                                        <span title="Đồng giá 599.000">Đồng giá 599.000</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-1 tab-content">
                                <div class="slick_ajax_tab products-view-grid">
                                </div>
                                <div class="alert alert-warning fade in green-alert" role="alert">
                                    <button type="button" class="close" data-dismiss="alert"><span
                                            aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                    Không có sản phẩm nào trong danh mục này.
                                </div>
                            </div>
                            <div class="tab-2 tab-content">
                            </div>
                            <div class="tab-3 tab-content">
                            </div>
                            <div class="tab-4 tab-content">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
