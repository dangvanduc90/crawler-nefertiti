<section class="awe-section-9">
    <div class="section_collection" data-aos="fade-up">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="grid-banner-type-5">
                        <div class="row slick-slider-mobile">
                            <div class="grid__item col-md-4 col-sm-4">
                                <div class="bg-effect">
                                    <a href="#" class="evo-fix-img" title="">
                                        <img
                                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"
                                            data-src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/evo_three_banner_collection_1.jpg?1624697016975"
                                            alt="" class="lazy img-responsive center-block"/>
                                    </a>
                                    <div class="featured-content clearfix">
                                        <h3><a href="#" title=""></a></h3>
                                    </div>
                                </div>
                            </div>
                            <div class="grid__item col-md-4 col-sm-4">
                                <div class="bg-effect">
                                    <a href="#" class="evo-fix-img" title="">
                                        <img
                                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"
                                            data-src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/evo_three_banner_collection_2.jpg?1624697016975"
                                            alt="" class="lazy img-responsive center-block"/>
                                    </a>
                                    <div class="featured-content clearfix">
                                        <h3><a href="#" title=""></a></h3>
                                    </div>
                                </div>
                            </div>
                            <div class="grid__item col-md-4 col-sm-4">
                                <div class="bg-effect">
                                    <a href="#" class="evo-fix-img" title="">
                                        <img
                                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"
                                            data-src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/evo_three_banner_collection_3.jpg?1624697016975"
                                            alt="" class="lazy img-responsive center-block"/>
                                    </a>
                                    <div class="featured-content clearfix">
                                        <h3><a href="#" title=""></a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
