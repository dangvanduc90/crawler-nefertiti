<section class="awe-section-3">
    <div class="section_san_pham_noi_bat" data-aos="zoom-out-up">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-6">
                    <div class="evo-ovr overlay_1">
                        <a href="{{ route('product.index') }}" class="fix-img">
                            <img class="lazy loaded"
                                 src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/evo_feature_index_1.jpg?1624697016975"
                                 data-src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/evo_feature_index_1.jpg?1624697016975"
                                 alt="Dịu dàng cùng Phanh Lee" data-was-processed="true">
                        </a>
                        <div class="ovrly"></div>
                        <div class="featured-content">
                            <p>Dịu dàng cùng Phanh Lee</p>
                            <h3>{{ config('app.short_name') }} x Diễn viên Phanh Lee</h3>
                            <a class="btn one" href="{{ route('product.index') }}" title="Mua ngay">Mua ngay</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-md-push-4 col-sm-4 col-sm-push-4 col-xs-6">
                    <div class="evo-ovr overlay_1">
                        <a href="{{ route('product.index') }}" class="fix-img">
                            <img class="lazy loaded"
                                 src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/evo_feature_index_3.jpg?1624697016975"
                                 data-src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/evo_feature_index_3.jpg?1624697016975"
                                 alt="Quyến rũ cùng My Sói" data-was-processed="true">
                        </a>
                        <div class="ovrly"></div>
                        <div class="featured-content">
                            <p>Quyến rũ cùng My Sói</p>
                            <h3>{{ config('app.short_name') }} x Diễn Viên Thu Quỳnh</h3>
                            <a class="btn one" href="{{ route('product.index') }}" title="Mua ngay">Mua ngay</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-md-pull-4 col-sm-4 col-sm-pull-4 col-xs-12">
                    <div class="evo-ovr overlay_2">
                        <a href="{{ route('product.index') }}" class="fix-img">
                            <img class="lazy img-responsive center-block loaded"
                                 src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/evo_feature_index_2.jpg?1624697016975"
                                 data-src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/evo_feature_index_2.jpg?1624697016975"
                                 alt="{{ config('app.name') }}" data-was-processed="true">
                        </a>
                        <div class="ovrly"></div>
                        <div class="featured-content">
                            <div class="featured-content-section">
                                <h5 class="h1">New Collection Comming Soon</h5>
                                <h5>
                                    <a class="btn one" href="{{ route('product.index') }}" title="Mua ngay">Mua
                                        ngay</a>
                                </h5></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
