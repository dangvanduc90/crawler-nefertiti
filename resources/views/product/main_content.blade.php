<section class="main_container collection col-md-9 col-sm-12 col-xs-12">
    <div class="evo-main-cate lazy hidden">
        <div class="group-category-white">
            <h1 class=" col-title">Tất cả sản phẩm</h1>
        </div>
    </div>
    <div class="category-products products category-products-grids clearfix">
        <div class="sort-cate clearfix hidden-xs">
            <div class="sort-cate-left hidden-xs">
                <h3>Xếp theo:</h3>
                <ul>
                    <li class="btn-quick-sort alpha-asc">
                        <a href="javascript:;" onclick="sortby('alpha-asc')" title="Tên A-Z"><i></i>Tên A-Z</a>
                    </li>
                    <li class="btn-quick-sort alpha-desc">
                        <a href="javascript:;" onclick="sortby('alpha-desc')" title="Tên Z-A"><i></i>Tên Z-A</a>
                    </li>
                    <li class="btn-quick-sort position-desc">
                        <a href="javascript:;" onclick="sortby('created-desc')" title="Hàng mới"><i></i>Hàng mới</a>
                    </li>
                    <li class="btn-quick-sort price-asc">
                        <a href="javascript:;" onclick="sortby('price-asc')" title="Giá thấp đến cao"><i></i>Giá thấp
                            đến cao</a>
                    </li>
                    <li class="btn-quick-sort price-desc">
                        <a href="javascript:;" onclick="sortby('price-desc')" title="Giá cao xuống thấp"><i></i>Giá cao
                            xuống thấp</a>
                    </li>
                </ul>
            </div>
        </div>
        <section class="products-view products-view-grid row">
            @foreach($products as $product)
                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
                <div class="evo-product-block-item">
                    <div class="product_thumb">
                        <a class="primary_img" href="{{ route('product.show', $product->slug) }}">
                            <img class="lazy"
                                 src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"
                                 data-src="{{ $product->image }}"
                                 alt="{{ $product->title }}"/>
                        </a>
                        <a class="secondary_img" href="{{ route('product.show', $product->slug) }}" title="{{ $product->title }}">
                            <img class="lazy"
                                 src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"
                                 data-src="{{ $product->image }}"
                                 alt="{{ $product->title }}"/>
                        </a>
                        <form action="{{ route('cart.store') }}" method="post" enctype="multipart/form-data"
                              class="cart-wrap hidden-sm hidden-xs hidden-md variants form-nut-grid form-ajaxtocart"
                              data-id="product-actions-{{ $product->id }}">
                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                            <input type="hidden" name="product_title" value="{{ $product->title }}">
                            <input type="hidden" name="product_price" value="{{ $product->price }}">
                            <button data-toggle="tooltip" data-placement="top" title="Thêm vào giỏ hàng" type="button"
                                    onclick="location.href='{{ route('product.show', $product->slug) }}'"
                                    class="action cart-button option-icons fas"></button>
                        </form>
                    </div>
                    <div class="product_content">
                        <div class="product_name">
                            <h4><a href="{{ route('product.show', $product->slug) }}" title="{{ $product->title }}">{{ $product->title }}</a></h4>
                        </div>
                        <div class="price-container">
                            <span class="current_price">{{ formatPrice($product->price) }}</span>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </section>
        @if(count($products) === 0)
            <div class="alert alert-warning alert-dismissible fade in margin-top-10 margin-left-10 margin-right-10 margin-bottom-10" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                Không có sản phẩm nào trong danh mục này.
            </div>
        @endif
    </div>
    <div class="clearfix"></div>
    <div class="text-xs-right">
        <nav class="text-center">
{{--            <ul class="pagination clearfix">--}}
{{--                <li class="page-item disabled"><a class="page-link" href="#" title="«">«</a></li>--}}
{{--                <li class="active page-item disabled"><a class="page-link" href="javascript:;" title="1">1</a></li>--}}
{{--                <li class="page-item"><a class="page-link" onclick="doSearch(2)" href="javascript:;" title="2">2</a>--}}
{{--                </li>--}}
{{--                <li class="page-item"><a class="page-link" onclick="doSearch(3)" href="javascript:;" title="3">3</a>--}}
{{--                </li>--}}
{{--                <li class="page-item"><a class="page-link" href="javascript:;" title="...">...</a></li>--}}
{{--                <li class="page-item"><a class="page-link" onclick="doSearch(34)" href="javascript:;" title="34">34</a>--}}
{{--                </li>--}}
{{--                <li class="page-item"><a class="page-link" onclick="doSearch(2)" href="javascript:;" title="»">»</a>--}}
{{--                </li>--}}
{{--            </ul>--}}
            {{ $products->links() }}
        </nav>
    </div>

</section>
