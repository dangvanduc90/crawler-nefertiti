@extends('layouts.app')
@section('title', $product->title)
@section('head')
    <link href='{{ asset('/css/evo-products.scss.css') }}' rel='stylesheet'>
    <link href='{{ asset('/css/picbox.css') }}' rel='stylesheet'>
    <link href='{{ asset('/css/bpr-products-module.css') }}' rel='stylesheet'>
@endsection
@section('schema')
    <script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "NewsArticle",
        "url": "{{ url()->current() }}",
        "mainEntityOfPage": {
            "@type": "WebPage",
            "@id": "{{ url()->current() }}"
        },
        "headline": "{{ $product->title }}",
        "image": "{{ $product->image }}",
        "datePublished": "{{ $product->created_at }}",
        "dateModified": "{{ $product->updated_at }}",
        "author": {
            "@type": "Person",
            "name": "{{ config('app.name') }}"
        },
        "publisher": {
            "@type": "Organization",
            "name": "{{ config('app.name') }}",
            "logo": {
                "@type": "ImageObject",
                "url": "{{ setting('logo') }}"
            }
        }
    }
</script>
<script type="application/ld+json">
        {
  "@context": "https://schema.org/",
  "@type": "Product",
  "name": "{{ $product->title }}",
  "image": [
    {!! implode(',', $product->pluck_images('"')) !!}
  ],
  "description": "{{ $product->title }}",
  "sku": "{{ $product->sku }}",
  "brand": {
    "@type": "Brand",
    "name": "{{ config('app.name') }}"
  },
  "mpn": "{{ $product->sku }}",
  "review": {
    "@type": "Review",
    "reviewRating": {
      "@type": "Rating",
      "ratingValue": "4.8",
      "bestRating": "5"
    },
    "author": {
      "@type": "Person",
      "name": "{{ config('app.name') }}"
    }
  },
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "4.8",
    "reviewCount": 15691
  },
  "offers": {
    "@type": "Offer",
    "url": "{{ url()->current() }}",
    "priceValidUntil": "2030-11-05",
    "priceCurrency": "VND",
    "price": "{{ $product->price }}",
    "itemCondition": "https://schema.org/UsedCondition",
    "availability": "https://schema.org/InStock",
    "seller": {
      "@type": "Organization",
      "name": "{{ config('app.name') }}"
    }
  }
}
    </script>
@endsection
@section('foot')
    <script src="{{ asset('/js/picbox.js') }}" type="text/javascript"></script>
@endsection
@section('body')
    {{ Breadcrumbs::render() }}
    <section class="product margin-top-10">
        <div class="container">
            <div class="row details-product padding-bottom-10">
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 product-bottom">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-lg-6 col-md-6">
                            <div class="relative product-image-block">
                                <div class="slider-big-video clearfix margin-bottom-10">
                                    <div class="slider slider-for">
                                        @foreach($product->images as $product_image)
                                        <a href="{{ $product_image->image }}" title="Click để xem">
                                            <img src="{{ $product_image->image }}" data-lazy="{{ $product_image->image }}" class="img-responsive center-block" alt="{{ $product->title }}" />
                                        </a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="slider-has-video clearfix">
                                    <div class="slider slider-nav">
                                        @foreach($product->images as $product_image)
                                        <div class="fixs">
                                            <img class="lazy" src="{{ $product_image->image }}" data-src="{{ $product_image->image }}" alt="{{ $product->title }}" />
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
{{--                                <div class="social-sharing margin-top-10">--}}
{{--                                    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a099baca270babc"></script>--}}
{{--                                    <div class="addthis_inline_share_toolbox_7dnb"></div>--}}
{{--                                </div>--}}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-6 col-md-6 details-pro">
                            <div class="product-top clearfix">
                                <h1 class="title-head">{{ $product->title }}</h1>
                                <div class="sku-product clearfix">
                                    <div class="item-sku">
                                        SKU: <span class="variant-sku">{{ $product->sku }}</span>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="price-box clearfix">
								<span class="special-price">
									<span class="price product-price">{{ formatPrice($product->price) }}</span>
								</span>
                                </div>
                                <div class="inventory_quantity">
                                    <span class="stock-brand-title">Tình trạng:</span>
                                    <span class="a-stock a1">Còn hàng</span>
                                </div>
                            </div>
                            <div class="form-product">
                                <form enctype="multipart/form-data" id="add-to-cart-form" action="{{ route('cart.store') }}" method="post" class="clearfix form-inline">
                                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                                    <input type="hidden" name="product_title" value="{{ $product->title }}">
                                    <input type="hidden" name="product_price" value="{{ $product->price }}">
                                    <div class="select-swatch">
                                        @if(count($product->sizes))
                                        <div id="variant-swatch-0" class="swatch clearfix" data-option="size_id" data-option-index="0">
                                            <div class="header">Kích thước:</div>
                                            <div class="select-swap">
                                                @foreach($product->sizes as $key => $size)
                                                <div data-value="{{ $size->title }}" class="n-sd swatch-element {{ $size->title }} ">
                                                    <input
                                                        data-value="{{ $size->title }}"
                                                        class="variant-0"
                                                        id="swatch-0-{{ $size->title }}"
                                                        type="radio"
                                                        name="size_id"
                                                        value="{{ $size->id }}"
                                                        @if($variant)
                                                            @if($variant->size_id == $size->id)
                                                                checked
                                                            @endif
                                                        @else
                                                            @if($key === 0)
                                                                checked
                                                            @endif
                                                        @endif
                                                    />
                                                    <label for="swatch-0-{{ $size->title }}">
                                                        {{ Str::upper($size->title) }}
                                                        <img class="crossed-out" src="{{ asset('/images/soldout.png') }}" alt="{{ $size->title }}" />
                                                        <img class="img-check" src="{{ asset('/images/select-pro.png') }}" alt="{{ $size->title }}" />
                                                    </label>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        @endif
                                        @if(count($product->colours) > 0)
                                        <div id="variant-swatch-1" class="swatch clearfix" data-option="color_id" data-option-index="1">
                                            <div class="header">Màu sắc:</div>
                                            <div class="select-swap">
                                                @foreach($product->colours as $key => $color)
                                                <div data-value="{{ $color->title }}" class="n-sd swatch-element color {{ $color->title }} ">
                                                    <input
                                                        data-value="{{ $color->title }}"
                                                        class="variant-1"
                                                        id="swatch-1-{{ $color->title }}"
                                                        type="radio"
                                                        name="color_id"
                                                        value="{{ $color->id }}"
                                                        @if($variant)
                                                            @if($variant->color_id == $color->id)
                                                                checked
                                                            @endif
                                                        @else
                                                            @if($key === 0)
                                                                checked
                                                            @endif
                                                        @endif
                                                    />
                                                    <label class="{{ $color->title }} no-thumb" for="swatch-1-{{ $color->title }}">
                                                        <span>{{ $color->title }}</span>
                                                        <img class="crossed-out" src="{{ asset('/images/soldout.png') }}" alt="{{ $color->title }}" />
                                                        <img class="img-check" src="{{ asset('/images/select-pro.png') }}" alt="{{ $color->title }}" />
                                                    </label>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        @endif
                                        @if($product->material)
                                        <div id="variant-swatch-2" class="swatch clearfix" data-option="material" data-option-index="2">
                                            <div class="header">Chất liệu:</div>
                                            <div class="select-swap">
                                                <div data-value="{{ $product->material }}" class="n-sd swatch-element {{ $product->material }} variant-3 ">
                                                    <input data-value="{{ $product->material }}" class="variant-2" id="swatch-2-{{ $product->material }}" type="radio" name="material" value="{{ $product->material }}" checked />
                                                    <label for="swatch-2-{{ $product->material }}">
                                                        {{ $product->material }}
                                                        <img class="crossed-out" src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/soldout.png?1624697016975" alt="{{ $product->material }}" />
                                                        <img class="img-check" src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/select-pro.png?1624697016975" alt="{{ $product->material }}" />
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
{{--                                    <div class="box-variant clearfix  hidden ">--}}
{{--                                        <select id="product-selectors" class="form-control form-control-lg" name="variantId" style="display:none">--}}
{{--                                            <option  selected="selected"  value="47454465">S / H&#7891;ng / Tuytsi Anh - {{ formatPrice($product->price) }}</option>--}}
{{--                                            <option  value="47454466">M / H&#7891;ng / Tuytsi Anh - {{ formatPrice($product->price) }}</option>--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
                                    <div class="clearfix form-group ">
                                        <div class="qty-ant clearfix custom-btn-number">
                                            <label>Số lượng:</label>
                                            <div class="custom custom-btn-numbers form-control">
                                                <button onclick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN(qty) & qty > 1 ) result.value--;return false;" class="btn-minus btn-cts" type="button">–</button>
                                                <input type="text" class="qty input-text" id="qty" name="quantity" size="4" value="1" maxlength="3" />
                                                <button onclick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN(qty)) result.value++;return false;" class="btn-plus btn-cts" type="button">+</button>
                                            </div>
                                        </div>
                                        <div class="btn-mua">
                                            <button type="submit" data-role='addtocart' class="btn btn-lg btn-gray btn-cart btn_buy add_to_cart">
                                                <span class="txt-main">Mua ngay với giá <b class="product-price">{{ formatPrice($product->price) }}</b></span>
                                                <span class="text-add">Đặt mua giao hàng tận nơi</span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="call-and-payment">
                                Hotline đặt hàng: <a href="tel:09.1234.0789" title="09.1234.0789"><i class="fa fa-phone-square" aria-hidden="true"></i> 09.1234.0789</a> (7:30-22:00)
                            </div>
                            <div class="store-place text-center">
                                <i class="fa fa-location-arrow" aria-hidden="true"></i> <span>Xem danh sách cửa hàng <a href='/he-thong-cua-hang' title='tại đây'>tại đây</a></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-20">
                    <div class="product-tab e-tabs padding-bottom-10 evo-tab-product-mobile">
                        <ul class="tabs tabs-title clearfix hidden-xs">
                            <li class="tab-link" data-tab="tab-1">Mô tả</li>
                            <li class="tab-link" data-tab="tab-2">Giới thiệu</li>
                        </ul>
                        <div id="tab-1" class="tab-content active">
                            <a class="evo-product-tabs-header hidden-lg hidden-md hidden-sm" href="javascript:void(0);">
                                <span>Mô tả</span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="5.658" height="9.903" viewBox="0 0 5.658 9.903">
                                    <path d="M5429 1331.94l4.451 4.451-4.451 4.452" stroke="#1c1c1c" stroke-linecap="round" fill="none" transform="translate(-5428.5 -1331.44)"></path>
                                </svg>
                            </a>
                            <div class="rte">
                                <p>Chất liệu: Tuytsi Anh&nbsp;</p>
                                <p>Đầm dáng ôm sát nách, đính cúc ngọc trai trang trí dọc thân trước, thắt nút ở eo tạo điểm nhấn</p>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-content">
                            <a class="evo-product-tabs-header hidden-lg hidden-md hidden-sm" href="javascript:void(0);">
                                <span>Giới thiệu</span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="5.658" height="9.903" viewBox="0 0 5.658 9.903">
                                    <path d="M5429 1331.94l4.451 4.451-4.451 4.452" stroke="#1c1c1c" stroke-linecap="round" fill="none" transform="translate(-5428.5 -1331.44)"></path>
                                </svg>
                            </a>
                            <div class="rte">
                                <p style="margin-bottom: 15px; text-align: justify;"><span style="font-size:11pt"><span style="background:white"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><i><span style="font-size:10.5pt"><span><span style="color:#333f48"><span style="letter-spacing:.2pt">Được thành lập vào ngày 19/04/2010 với tên giao dịch đầy đủ Công ty TNHH Ngọc Kim (</span></span></span></span></i><span style="font-size:10.0pt"><span style="background:#f0f0f0"><span><span style="color:#333333">NGOC KIM COMPANY LIMITED</span></span></span></span><i><span style="font-size:10.5pt"><span><span style="color:#333f48"><span style="letter-spacing:.2pt"> ) tên viết tắt Ngockim CO.,LTD &nbsp;&nbsp;hoạt động trong lĩnh vực thiết kế và sản xuất thời trang may mặc phân phối trên toàn quốc với 2 thương hiệu thời trang nổi tiếng giành cho Nữ và Nam.</span></span></span></span></i></span></span></span></span></p>
                                <p style="margin-bottom: 15px; text-align: justify;"><span style="font-size:11pt"><span style="background:white"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><i><span style="font-size:10.5pt"><span><span style="color:#333f48"><span style="letter-spacing:.2pt">{{ config('app.short_name') }} – Thời trang công sở nữ</span></span></span></span></i></span></span></span></span></p>
                                <p style="margin-bottom: 15px; text-align: justify;"><span style="font-size:11pt"><span style="background:white"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><i><span style="font-size:10.5pt"><span><span style="color:#333f48"><span style="letter-spacing:.2pt">Mancarini – Thời trang công sở Nam</span></span></span></span></i></span></span></span></span></p>
                                <p style="text-align: justify;"><em>{{ config('app.short_name') }} là một trong những thương hiệu thời trang công sở nổi tiếng tại Việt Nam. Thời trang {{ config('app.short_name') }} nổi tiếng với sự thanh lịch, sang trọng, dành cho những khách hàng là nhân viên văn phòng, công chức, nữ doanh nhân và những người phụ nữ hiện đại nói chung. Với kim chỉ nam “Khơi dậy vẻ đẹp tiềm ẩn” trong sứ mệnh tỏa sáng vẻ đẹp phụ nữ Việt Nam, Nefertiti đã và đang khẳng định được sự vượt trội và nổi bật của mình.</em></p>
                                <p style="margin-bottom: 15px; text-align: justify;"><span style="font-size:11pt"><span style="background:white"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><i><span style="font-size:10.5pt"><span><span style="color:#333f48"><span style="letter-spacing:.2pt">Thương hiệu thời trang&nbsp;Mancarini là thương hiệu thời trang Nam do Ngọc Kim&nbsp;phát triển. Lấy cảm hứng từ sự lịch lãm và sang trọng của người đàn ông ITALY, Với sự đầu tư cải tiến công nghệ dệt, may, chú trọng các khâu thiết kế mẫu mã, đến việc thiết lập xây dựng mạnh mẽ hệ thống kênh phân phối sản phẩm trong và ngoài nước, đến nay, thương hiệu thời trang&nbsp;Mancarini đã trở thành một trong những thương hiệu dành cho nam giới được nhiều khách hàng quan tâm lựa chọn, đặc biệt là khi có nhu cầu trang phục thời trang công sở, thể thao nam.</span></span></span></span></i></span></span></span></span></p>
                                <p style="text-align: justify;"><strong>ĐƠN VỊ ĐẦU TIÊN ỨNG DỤNG CÔNG NGHỆ MULTINANO</strong></p>
                                <p style="text-align: justify;">Ngọc Kim&nbsp;tự hào là đơn vị thời trang đầu tiên ở Việt Nam, tiên phong nghiên cứu và áp dụng công nghệ Multinano vào thiết kế thời trang. Đặc điểm nổi bật của công nghệ này chính là khả năng kháng khuẩn rất tốt - khử mùi hôi - chống nấm mốc - giảm tác hại của tia UV. Điều này chứng tỏ sự sáng tạo vượt trội trong công nghệ của Ngọc Kim&nbsp;cũng như một giá trị tốt đẹp trong sứ mệnh đem lại những sản phẩm vừa thời trang, vừa bảo vệ sức khỏe cho người sử dụng<img data-thumb="original" original-height="960" original-width="698" src="//bizweb.dktcdn.net/100/404/112/files/vien-pastel.jpg?v=1602236621884" alt="{{ config('app.name') }}" /></p>
                                <p style="text-align: justify;"><em>Giấy chứng nhận chất lượng vải ứng dụng công nghệ Multinano</em></p>
                                <p style="text-align: justify;">&nbsp;</p>
                                <p style="text-align: justify;"><strong>KỸ THUẬT TẠO PHOM DÁNG CHUẨN</strong></p>
                                <p style="text-align: justify;">&nbsp;</p>
                                <p style="text-align: justify;">Một điều đặc biệt trong các thiết kế của Ngọc Kim, chính là kỹ thuật tạo phom dáng chuẩn. Với đội ngũ thiết kế và đội ngũ sản xuất giàu kinh nghiệm đã tạo nên các thiết kế hoàn hảo, các đường cắp cúp tinh xảo ôm phom, tôn dáng. Khách hàng sở hữu sản phẩm của Ngọc Kim&nbsp;sẽ cảm thấy hài lòng vì cảm giác như thiết kế riêng cho mình. Kỹ thuật tạo phom dáng chuẩn cũng là yếu tố quan trọng để tạo ra sự khác biệt, bảo vệ danh tiếng và giữ chân khách hàng thân thiết của Ngọc Kim&nbsp;suốt gần 10&nbsp;năm qua.</p>
                                <p style="text-align: justify;">&nbsp;</p>
                                <p style="text-align: justify;"><img data-thumb="original" original-height="1200" original-width="831" src="//bizweb.dktcdn.net/100/404/112/files/nefertiti-va-my-soi-687e4f95-8684-43a6-abbf-5ddbbbe8d9ee.jpg?v=1602236869401" alt="Diễn viên My Sói" /><em>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;CEO Ngọc Bích cùng diễn viên Thu Quỳnh<br />
                                        <img data-thumb="original" original-height="606" original-width="928" src="//bizweb.dktcdn.net/100/404/112/files/dien-vien-thu-huong-lan-cave.jpg?v=1602236924989" alt="Diễn viên Thu Hương" />Diễn viên Thu Hương ra mắt BST The Muse by {{ config('app.short_name') }}&nbsp;</em></p>
                            </div>
                        </div>
                    </div>
                    <div id="top-tabs-info" class="">
                        <div class="productAnchor_horizonalNavs">
                            <div class="productAnchor_horizonalNav">
                                <div class="product_info_image">
                                    <img class="pict imagelazyload" src="{{ $product->image }}" alt="{{ $product->title }}" />
                                </div>
                                <div class="product_info_content">
                                    <h2 class="product_info_name" title="{{ $product->title }}">{{ $product->title }}</h2>
                                    <div class="product_info_price">
                                        <div class="product_info_price_title">
                                            Giá bán:
                                        </div>
                                        <div class="product_info_price_value">
                                            <div class="product_info_price_value-final">{{ formatPrice($product->price) }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_info_buttons">
                                    <button class="btn btn_buyNow btn-buy-now-click">
                                        <span class="txt-main">MUA NGAY</span>
                                        <span class="txt-sub">Giao hàng tận nơi</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('product.related', ['productRelated' => $productRelated, 'category' => $category])
        </div>
    </section>
    <script>
        $(document).ready(function ($) {
            jQuery(document).ready(function(e) {
                var WindowHeight = jQuery(window).height();
                var load_element = 0;
                //position of element
                var scroll_position = jQuery('.product-bottom').offset().top + jQuery('.product-bottom').outerHeight(true);;
                var screen_height = jQuery(window).height();
                var activation_offset = 0;
                var max_scroll_height = jQuery('body').height() + screen_height;
                var scroll_activation_point = scroll_position - (screen_height * activation_offset);
                jQuery(window).on('scroll', function(e) {
                    var y_scroll_pos = window.pageYOffset;
                    var element_in_view = y_scroll_pos > scroll_activation_point;
                    var has_reached_bottom_of_page = max_scroll_height <= y_scroll_pos && !element_in_view;
                    if (element_in_view || has_reached_bottom_of_page) {
                        jQuery('.productAnchor_horizonalNavs').addClass("ins-Drop");
                    } else {
                        jQuery('.productAnchor_horizonalNavs').removeClass("ins-Drop");
                    }
                });
            });
        });
    </script>
    <script>
        $(document).on('click', '.btn-buy-now-click', function(e) {
            e.preventDefault();
            $('[data-role=addtocart]').click();
        });
        $('.slider-nav').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: false,
            centerMode: false,
            infinite: false,
            focusOnSelect: true,
            responsive: [
                {
                    breakpoint: 1025,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 4
                    }
                }
            ]
        });
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            lazyLoad: 'ondemand',
            fade: true,
            infinite: false,
            asNavFor: '.slider-nav',
            adaptiveHeight: false,
            responsive: [
                {
                    breakpoint: 480,
                    settings:{
                        dots: true
                    }
                }
            ]
        });
        $('.slider-big-video .slider-for a').each(function() {
            $(this).attr('rel','lightbox-demo');
        });
        jQuery('.swatch :radio').change(function() {
            var optionIndex = jQuery(this).closest('.swatch').attr('data-option-index');
            var optionValue = jQuery(this).val();
            jQuery(this)
                .closest('form')
                .find('.single-option-selector')
                .eq(optionIndex)
                .val(optionValue)
                .trigger('change');
        });
        function scrollToxx() {
            if ($(window).width() > 767) {
                $('html, body').animate({ scrollTop: $('.product-tab.e-tabs').offset().top }, 'slow');
                $('.tab-content, .product-tab .tab-link').removeClass('current');
                $('#tab-3, .product-tab .tab-link:nth-child(3)').addClass('current');
                return false;
            }else{
                $('html, body').animate({ scrollTop: $('.product-tab.e-tabs #tab-3').offset().top }, 'slow');
                $('.product-tab.e-tabs #tab-3').addClass('active');
            }
        }
        function scrollToxxs() {
            $('html, body').animate({ scrollTop: $('.product-tab.e-tabs').offset().top }, 'slow');
            return false;
        }
        $('.btn--view-more .less-text').click(function(){
            scrollToxxs();
        })
        if ($(window).width() < 767) {
            $('.evo-tab-product-mobile .tab-content .evo-product-tabs-header').on('click', function(e){
                e.preventDefault();
                var $this = $(this);
                $this.parents('.evo-tab-product-mobile .tab-content').find('.rte').stop().slideToggle();
                $(this).parent().toggleClass('active')
                return false;
            });
        }
        $('.evo-product-summary .evo-product-tabs-header').on('click', function(e){
            e.preventDefault();
            var $this = $(this);
            $this.parents('.evo-product-summary').find('.rte-summary').stop().slideToggle();
            $(this).parent().toggleClass('active')
            return false;
        });
    </script>
    <div class="sapo-product-reviews-module"></div>
@endsection
