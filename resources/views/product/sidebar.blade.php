<aside class="sidebar left-content col-md-3 col-sm-12 col-xs-12">
    <script src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/search_filter.js?1624697016975"
            type="text/javascript"></script>
    <div class="aside-filter clearfix">
        <div class="heading hidden-lg hidden-md">Lọc sản phẩm <i class="fas fa-plus"></i></div>
        <div class="aside-hidden-mobile">
            <div class="filter-container">
                <div class="filter-containers hidden">
                    <div class="filter-container__selected-filter" style="display: none;">
                        <div class="filter-container__selected-filter-list clearfix">
                            <ul>
                            </ul>
                            <a href="javascript:void(0)" onclick="clearAllFiltered()"
                               class="filter-container__clear-all" title="Bỏ hết">Bỏ hết</a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <aside class="aside-item filter-price">
                    <div class="aside-title">Giá sản phẩm <i class="fas fa-minus"></i></div>
                    <div class="aside-content filter-group">
                        <ul>
                            <li class="filter-item filter-item--check-box filter-item--green">
							<span>
								<label for="filter-duoi-100-000d">
									<input type="checkbox" id="filter-duoi-100-000d" onchange="toggleFilter(this);"
                                           data-group="Khoảng giá" data-field="price_min" data-text="Dưới 100.000đ"
                                           value="(<100000)" data-operator="OR">
									<i class="fa"></i>
									Giá dưới 100.000đ
								</label>
							</span>
                            </li>
                            <li class="filter-item filter-item--check-box filter-item--green">
							<span>
								<label for="filter-100-000d-200-000d">
									<input type="checkbox" id="filter-100-000d-200-000d" onchange="toggleFilter(this)"
                                           data-group="Khoảng giá" data-field="price_min"
                                           data-text="100.000đ - 200.000đ" value="(>100000 AND <200000)"
                                           data-operator="OR">
									<i class="fa"></i>
									100.000đ - 200.000đ
								</label>
							</span>
                            </li>
                            <li class="filter-item filter-item--check-box filter-item--green">
							<span>
								<label for="filter-200-000d-300-000d">
									<input type="checkbox" id="filter-200-000d-300-000d" onchange="toggleFilter(this)"
                                           data-group="Khoảng giá" data-field="price_min"
                                           data-text="200.000đ - 300.000đ" value="(>200000 AND <300000)"
                                           data-operator="OR">
									<i class="fa"></i>
									200.000đ - 300.000đ
								</label>
							</span>
                            </li>
                            <li class="filter-item filter-item--check-box filter-item--green">
							<span>
								<label for="filter-300-000d-500-000d">
									<input type="checkbox" id="filter-300-000d-500-000d" onchange="toggleFilter(this)"
                                           data-group="Khoảng giá" data-field="price_min"
                                           data-text="300.000đ - 500.000đ" value="(>300000 AND <500000)"
                                           data-operator="OR">
									<i class="fa"></i>
									300.000đ - 500.000đ
								</label>
							</span>
                            </li>
                            <li class="filter-item filter-item--check-box filter-item--green">
							<span>
								<label for="filter-500-000d-1-000-000d">
									<input type="checkbox" id="filter-500-000d-1-000-000d" onchange="toggleFilter(this)"
                                           data-group="Khoảng giá" data-field="price_min"
                                           data-text="500.000đ - 1.000.000đ" value="(>500000 AND <1000000)"
                                           data-operator="OR">
									<i class="fa"></i>
									500.000đ - 1.000.000đ
								</label>
							</span>
                            </li>
                            <li class="filter-item filter-item--check-box filter-item--green">
							<span>
								<label for="filter-tren1-000-000d">
									<input type="checkbox" id="filter-tren1-000-000d" onchange="toggleFilter(this)"
                                           data-group="Khoảng giá" data-field="price_min" data-text="Trên 1.000.000đ"
                                           value="(>1000000)" data-operator="OR">
									<i class="fa"></i>
									Giá trên 1.000.000đ
								</label>
							</span>
                            </li>
                        </ul>
                    </div>
                </aside>
                <aside class="aside-item filter-type">
                    <div class="aside-title">Loại <i class="fas fa-minus"></i></div>
                    <div class="aside-content filter-group">
                        <ul class="filter-type">
                            <li class="filter-item filter-item--check-box filter-item--green">
                                <label data-filter="áo phông cộc tay" for="filter-ao-phong-coc-tay">
                                    <input type="checkbox" id="filter-ao-phong-coc-tay" onchange="toggleFilter(this)"
                                           data-group="Loại" data-field="product_type.filter_key"
                                           data-text="Áo phông cộc tay" value="(&#34;Áo phông cộc tay&#34;)"
                                           data-operator="OR">
                                    <i class="fa"></i>
                                    Áo phông cộc tay
                                </label>
                            </li>
                            <li class="filter-item filter-item--check-box filter-item--green">
                                <label data-filter="đầm hè" for="filter-dam-he">
                                    <input type="checkbox" id="filter-dam-he" onchange="toggleFilter(this)"
                                           data-group="Loại" data-field="product_type.filter_key" data-text="Đầm hè"
                                           value="(&#34;Đầm hè&#34;)" data-operator="OR">
                                    <i class="fa"></i>
                                    Đầm hè
                                </label>
                            </li>
                            <li class="filter-item filter-item--check-box filter-item--green">
                                <label data-filter="đồng giá" for="filter-dong-gia">
                                    <input type="checkbox" id="filter-dong-gia" onchange="toggleFilter(this)"
                                           data-group="Loại" data-field="product_type.filter_key" data-text="Đồng giá"
                                           value="(&#34;Đồng giá&#34;)" data-operator="OR">
                                    <i class="fa"></i>
                                    Đồng giá
                                </label>
                            </li>
                            <li class="filter-item filter-item--check-box filter-item--green">
                                <label data-filter="hàng đông" for="filter-hang-dong">
                                    <input type="checkbox" id="filter-hang-dong" onchange="toggleFilter(this)"
                                           data-group="Loại" data-field="product_type.filter_key" data-text="Hàng đông"
                                           value="(&#34;Hàng đông&#34;)" data-operator="OR">
                                    <i class="fa"></i>
                                    Hàng đông
                                </label>
                            </li>
                            <li class="filter-item filter-item--check-box filter-item--green">
                                <label data-filter="hàng hè" for="filter-hang-he">
                                    <input type="checkbox" id="filter-hang-he" onchange="toggleFilter(this)"
                                           data-group="Loại" data-field="product_type.filter_key" data-text="Hàng hè"
                                           value="(&#34;Hàng hè&#34;)" data-operator="OR">
                                    <i class="fa"></i>
                                    Hàng hè
                                </label>
                            </li>
                            <li class="filter-item filter-item--check-box filter-item--green">
                                <label data-filter="hàng hóa" for="filter-hang-hoa">
                                    <input type="checkbox" id="filter-hang-hoa" onchange="toggleFilter(this)"
                                           data-group="Loại" data-field="product_type.filter_key" data-text="Hàng Hóa"
                                           value="(&#34;Hàng Hóa&#34;)" data-operator="OR">
                                    <i class="fa"></i>
                                    Hàng Hóa
                                </label>
                            </li>
                            <li class="filter-item filter-item--check-box filter-item--green">
                                <label data-filter="hàng thái" for="filter-hang-thai">
                                    <input type="checkbox" id="filter-hang-thai" onchange="toggleFilter(this)"
                                           data-group="Loại" data-field="product_type.filter_key" data-text="Hàng Thái"
                                           value="(&#34;Hàng Thái&#34;)" data-operator="OR">
                                    <i class="fa"></i>
                                    Hàng Thái
                                </label>
                            </li>
                            <li class="filter-item filter-item--check-box filter-item--green">
                                <label data-filter="hàng thu" for="filter-hang-thu">
                                    <input type="checkbox" id="filter-hang-thu" onchange="toggleFilter(this)"
                                           data-group="Loại" data-field="product_type.filter_key" data-text="Hàng thu"
                                           value="(&#34;Hàng thu&#34;)" data-operator="OR">
                                    <i class="fa"></i>
                                    Hàng thu
                                </label>
                            </li>
                            <li class="filter-item filter-item--check-box filter-item--green">
                                <label data-filter="hàng xuân" for="filter-hang-xuan">
                                    <input type="checkbox" id="filter-hang-xuan" onchange="toggleFilter(this)"
                                           data-group="Loại" data-field="product_type.filter_key" data-text="Hàng Xuân"
                                           value="(&#34;Hàng Xuân&#34;)" data-operator="OR">
                                    <i class="fa"></i>
                                    Hàng Xuân
                                </label>
                            </li>
                            <li class="filter-item filter-item--check-box filter-item--green">
                                <label data-filter="kích thước" for="filter-kich-thuoc">
                                    <input type="checkbox" id="filter-kich-thuoc" onchange="toggleFilter(this)"
                                           data-group="Loại" data-field="product_type.filter_key" data-text="Kích thước"
                                           value="(&#34;Kích thước&#34;)" data-operator="OR">
                                    <i class="fa"></i>
                                    Kích thước
                                </label>
                            </li>
                            <li class="filter-item filter-item--check-box filter-item--green">
                                <label data-filter="phụ kiện" for="filter-phu-kien">
                                    <input type="checkbox" id="filter-phu-kien" onchange="toggleFilter(this)"
                                           data-group="Loại" data-field="product_type.filter_key" data-text="Phụ Kiện"
                                           value="(&#34;Phụ Kiện&#34;)" data-operator="OR">
                                    <i class="fa"></i>
                                    Phụ Kiện
                                </label>
                            </li>
                            <li class="filter-item filter-item--check-box filter-item--green">
                                <label data-filter="thành phẩm" for="filter-thanh-pham">
                                    <input type="checkbox" id="filter-thanh-pham" onchange="toggleFilter(this)"
                                           data-group="Loại" data-field="product_type.filter_key" data-text="Thành Phẩm"
                                           value="(&#34;Thành Phẩm&#34;)" data-operator="OR">
                                    <i class="fa"></i>
                                    Thành Phẩm
                                </label>
                            </li>
                        </ul>
                    </div>
                </aside>
                <aside class="aside-item filter-tag-style-1 tag-filtster">
                    <div class="aside-title">Kích thước <i class="fas fa-minus"></i></div>
                    <div class="aside-content filter-group">
                        <ul>
                            <li class="filter-item filter-item--check-box filter-item--green">
							<span>
								<label for="filter-lon">
									<input type="checkbox" id="filter-lon" onchange="toggleFilter(this)"
                                           data-group="tag2" data-field="tags" data-text="Lớn" value="(Lớn)"
                                           data-operator="OR">
									<i class="fa"></i>
									Lớn
								</label>
							</span>
                            </li>
                            <li class="filter-item filter-item--check-box filter-item--green">
							<span>
								<label for="filter-nho">
									<input type="checkbox" id="filter-nho" onchange="toggleFilter(this)"
                                           data-group="tag2" data-field="tags" data-text="Nhỏ" value="(Nhỏ)"
                                           data-operator="OR">
									<i class="fa"></i>
									Nhỏ
								</label>
							</span>
                            </li>
                            <li class="filter-item filter-item--check-box filter-item--green">
							<span>
								<label for="filter-vua">
									<input type="checkbox" id="filter-vua" onchange="toggleFilter(this)"
                                           data-group="tag2" data-field="tags" data-text="Vừa" value="(Vừa)"
                                           data-operator="OR">
									<i class="fa"></i>
									Vừa
								</label>
							</span>
                            </li>
                        </ul>
                    </div>
                </aside>
                <aside class="aside-item filter-tag-style-1 tag-filtster">
                    <div class="aside-title">Họa tiết <i class="fas fa-minus"></i></div>
                    <div class="aside-content filter-group">
                        <ul>
                            <li class="filter-item filter-item--check-box filter-item--green">
							<span>
								<label for="filter-hoa-ke-caro">
									<input type="checkbox" id="filter-hoa-ke-caro" onchange="toggleFilter(this)"
                                           data-group="tag3" data-field="tags" data-text="hoa,kẻ caro"
                                           value="(hoa,kẻ caro)" data-operator="OR">
									<i class="fa"></i>
									hoa,kẻ caro
								</label>
							</span>
                            </li>
                        </ul>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</aside>
