@if(count($products) > 0)
    <div>
        <div class="slick_ajax_tab products-view-grid">
            @foreach($products as $product)
                <div class="item">
                <div class="evo-product-block-item">
                    <div class="product_thumb">
                        <a class="primary_img" href="{{ route('product.show', $product->slug) }}">
                            <img class="lazy" src="{{ $product->image }}" data-src="{{ $product->image }}" alt="{{ $product->title }}" />
                        </a>
                        <a class="secondary_img" href="{{ route('product.show', $product->slug) }}" title="{{ $product->title }}">
                            <img class="lazy" src="{{ $product->image }}" data-src="{{ $product->image }}" alt="{{ $product->title }}" />
                        </a>
                        <form action="{{ route('cart.store') }}" method="post" enctype="multipart/form-data" class="cart-wrap hidden-sm hidden-xs hidden-md variants form-nut-grid form-ajaxtocart" data-id="product-actions-{{ $product->id }}">
                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                            <input type="hidden" name="product_title" value="{{ $product->title }}">
                            <input type="hidden" name="product_price" value="{{ $product->price }}">
                            <button data-toggle="tooltip" data-placement="top" title="Thêm vào giỏ hàng" type="button" onclick="location.href='{{ route('product.show', $product->slug) }}'" class="action cart-button option-icons fas"></button>
                        </form>
                    </div>
                    <div class="product_content">
                        <div class="product_name">
                            <h4><a href="{{ route('product.show', $product->slug) }}" title="{{ $product->title }}">{{ $product->title }}</a></h4>
                        </div>
                        <div class="price-container">
                            <span class="current_price">{{ formatPrice($product->price) }}</span>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
@else
    <div>
        <div class="fw bg-warning padding-15 noproduct" >
            <div class="text-warning">
                Không có sản phẩm nào trong danh mục này.
            </div>
        </div>
    </div>
@endif
