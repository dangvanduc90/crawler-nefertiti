@extends('layouts.app')
@section('title', 'Tất cả sản phẩm')
@section('body')
    {{ Breadcrumbs::render() }}
    <div class="container">
        <div class="row">
            @include('product.sidebar')
            @include('product.main_content', $products)
        </div>
    </div>
@endsection
