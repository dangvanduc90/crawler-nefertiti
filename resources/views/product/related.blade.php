@if($category)
<div class="row margin-top-10 margin-bottom-10">
    <div class="col-lg-12">
        <div class="related-product product-white-bg">
            <div class="home-title text-center">
                <h2><a href="{{ route('category.show', $category->slug) }}" title="Sản phẩm liên quan">Sản phẩm liên quan</a></h2>
            </div>
            <div class="evo-owl-product clearfix">
                @foreach($productRelated as $product)
                <div class="evo-slick">
                    <div class="evo-product-block-item">
                        <div class="product_thumb">
                            <a class="primary_img" href="{{ route('product.show', $product->slug) }}">
                                <img class="lazy" src="{{ $product->image }}" data-src="{{ $product->image }}" alt="{{ $product->title }}" />
                            </a>
                            <a class="secondary_img" href="{{ route('product.show', $product->slug) }}" title="{{ $product->title }}">
                                <img class="lazy" src="{{ $product->image }}" data-src="{{ $product->image }}" alt="{{ $product->title }}" />
                            </a>
                            <form action="{{ route('cart.store') }}" method="post" enctype="multipart/form-data" class="cart-wrap hidden-sm hidden-xs hidden-md variants form-nut-grid form-ajaxtocart" data-id="product-actions-{{ $product->id }}">
                                <input type="hidden" name="product_id" value="{{ $product->id }}">
                                <input type="hidden" name="product_title" value="{{ $product->title }}">
                                <input type="hidden" name="product_price" value="{{ $product->price }}">
                                <button data-toggle="tooltip" data-placement="top" title="Thêm vào giỏ hàng" type="button" onclick="location.href='{{ route('product.show', $product->slug) }}'" class="action cart-button option-icons fas"></button>
                            </form>
                        </div>
                        <div class="product_content">
                            <div class="product_name">
                                <h4><a href="{{ route('product.show', $product->slug) }}" title="{{ $product->title }}">{{ $product->title }}</a></h4>
                            </div>
                            <div class="price-container">
                                <span class="current_price">{{ formatPrice($product->price) }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<script>
    $('document').ready(function () {
        $('.evo-owl-product').slick({
            dots: true,
            arrows: false,
            infinite: false,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 5,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }
            ]
        });
    })
</script>
@endif
