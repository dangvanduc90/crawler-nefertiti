@extends('layouts.app')
@section('title', 'Cảm ơn bạn đã tin tưởng chúng tôi')
@section('head')
    <link href='{{ asset('/css/order.css') }}' rel='stylesheet'>
@endsection
@section('body')
    <div class="text-center thankyou-page container">
        <h4>Cảm ơn bạn đã tin tưởng chúng tôi!</h4>
        <p>Một email xác nhận sẽ được gửi tới hòm thư của bạn ngay.</p>
        <a href="{{ route('order.show', $order->code) }}" class="btn btn-success">Xem lại chi tiết đơn hàng</a>
    </div>
@endsection
