@extends('layouts.app')
@section('title', 'Chi tiết đơn hàng')
@section('head')
    <link href='{{ asset('/css/evo-carts.scss.css') }}' rel='stylesheet'>
    <link href='{{ asset('/css/order.css') }}' rel='stylesheet'>
@endsection
@section('body')
    {{ Breadcrumbs::render() }}
    <div class="container white collections-container margin-bottom-20 margin-top-30">
        <div class="white-background">
            <div class="row">
                <div class="col-md-12">
                    <div class="shopping-cart">
                        <div class="visible-md visible-lg">
                            <div class="shopping-cart-table">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h1 class="lbl-shopping-cart lbl-shopping-cart-gio-hang">Giỏ hàng <span>(<span
                                                    class="count_item_pr">{{ $order->total_quantity }}</span> sản phẩm)</span></h1>
                                    </div>
                                </div>
                                <div class="row">
                                    @if(count($order_items) > 0)
                                        <div class="col-main cart_desktop_page cart-page">
                                            <form id="shopping-cart" action="{{ route('cart.index') }}" method="post" novalidate>
                                                <div class="cart page_cart cart_des_page hidden-xs-down">
                                                    <div class="col-xs-9 cart-col-1">
                                                        <div class="cart-tbody">
                                                            @foreach($order_items as $order_item)
                                                                <div class="row shopping-cart-item productid-{{ $order_item->id }}">
                                                                    <div class="col-xs-3 img-thumnail-custom"><p class="image">
                                                                            <a href="{{ route('product.show', $order_item->product_variant->product->slug) }}"
                                                                               title="{{ $order_item->product_variant->product->title }} - {{ $order_item->product_variant->attr_values }}"
                                                                               target="_blank"><img class="img-responsive"
                                                                                                    src="{{ $order_item->product_variant->product->image }}"
                                                                                                    alt="{{ $order_item->product_variant->product->title }} - {{ $order_item->product_variant->attr_values }}"></a>
                                                                        </p></div>
                                                                    <div class="col-right col-xs-9">
                                                                        <div class="box-info-product"><p class="name"><a
                                                                                    href="{{ route('product.show', $order_item->product_variant->product->slug) }}"
                                                                                    title="{{ $order_item->product_variant->product->title }} - {{ $order_item->product_variant->attr_values }}"
                                                                                    target="_blank">{{ $order_item->product_variant->product->title }} - {{ $order_item->product_variant->attr_values }}</a></p>
                                                                            <p class="c-brands">Thương hiệu: {{ $order_item->product_variant->product->trademark }}</p>
                                                                            <p class="seller-by hidden">M</p>
                                                                            <form action="{{ route('checkout.repurchase.item') }}" method="post">
                                                                                @csrf
                                                                                <input type="hidden" name="variant_id" value="{{ $order_item->product_variant->id }}">
                                                                                <input type="hidden" name="product_id" value="{{ $order_item->product_variant->product_id }}">
                                                                                <input type="hidden" name="size_id" value="{{ $order_item->product_variant->size_id }}">
                                                                                <input type="hidden" name="color_id" value="{{ $order_item->product_variant->color_id }}">
                                                                                <input type="hidden" name="product_title" value="{{ $order_item->product_variant->product->title }}">
                                                                                <input type="hidden" name="quantity" value="{{ $order_item->quantity }}">
                                                                                <input type="hidden" name="product_price" value="{{ $order_item->product_variant->product->price }}">
                                                                                <button class="btn-link"><u>Mua lại</u></button>
                                                                            </form>
                                                                        </div>
                                                                        <div class="box-price"><p class="price pricechange" data-price="{{ $order_item->price }}">{{ formatPrice($order_item->price) }}</p></div>
                                                                        <div class="quantity-block">
                                                                            <div class="input-group bootstrap-touchspin">
                                                                                <div class="input-group-btn"><input
                                                                                        class="variantID" type="hidden"
                                                                                        name="variantId" value="{{ $order_item->id }}">
                                                                                    <input type="text"
                                                                                           onchange="if(this.value == 0)this.value=1;"
                                                                                           maxlength="12"
                                                                                           min="1"
                                                                                           readonly
                                                                                           class="form-control quantity-r2 quantity js-quantity-product input-text number-sidebar input_pop input_pop qtyItem{{ $order_item->id }}"
                                                                                           id="qtyItem{{ $order_item->id }}"
                                                                                           name="Lines"
                                                                                           size="4"
                                                                                           value="{{ $order_item->quantity }}">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-3 cart-col-2 cart-collaterals cart_submit">
                                                        <div id="right-affix">
                                                            <div class="each-row">
                                                                <div class="box-style fee"><p class="list-info-price"><span>Tạm tính: </span><strong
                                                                            class="totals_price price _text-right text_color_right1">{{ formatPrice($order->total_price) }}</strong>
                                                                    </p></div>
                                                                <div class="box-style fee">
                                                                    <div class="total2 clearfix"><span class="text-label">Thành tiền: </span>
                                                                        <div class="amount"><p><strong class="totals_price">{{ formatPrice($order->total_price) }}</strong>
                                                                            </p></div>
                                                                    </div>
                                                                </div>
                                                                <form action="{{ route('checkout.repurchase.order') }}" method="post">
                                                                    @csrf
                                                                    @foreach($order_items as $order_item)
                                                                        <input type="hidden" name="{{ $order_item->product_variant->id }}[variant_id]" value="{{ $order_item->product_variant->id }}">
                                                                        <input type="hidden" name="{{ $order_item->product_variant->id }}[product_id]" value="{{ $order_item->product_variant->product_id }}">
                                                                        <input type="hidden" name="{{ $order_item->product_variant->id }}[size_id]" value="{{ $order_item->product_variant->size_id }}">
                                                                        <input type="hidden" name="{{ $order_item->product_variant->id }}[color_id]" value="{{ $order_item->product_variant->color_id }}">
                                                                        <input type="hidden" name="{{ $order_item->product_variant->id }}[product_title]" value="{{ $order_item->product_variant->product->title }}">
                                                                        <input type="hidden" name="{{ $order_item->product_variant->id }}[quantity]" value="{{ $order_item->quantity }}">
                                                                        <input type="hidden" name="{{ $order_item->product_variant->id }}[product_price]" value="{{ $order_item->product_variant->product->price }}">
                                                                    @endforeach
                                                                    <button class="button btn btn-large btn-block btn-danger btn-checkout evo-button"><u>Mua lại</u></button>
                                                                </form>
                                                                <button
                                                                    class="button btn-proceed-checkout btn btn-large btn-block btn-danger btn-checkouts"
                                                                    title="Tiếp tục mua hàng" type="button"
                                                                    onclick="window.location.href='/'">Trở về trang chủ
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="visible-sm visible-xs">
                            @if(count($order_items) > 0)
                                <div class="cart-mobile">
                                    <form action="{{ route('cart.index') }}" method="post" class="margin-bottom-0">
                                        <div class="header-cart">
                                            <div class="title-cart clearfix">
                                                <h3>Giỏ hàng của bạn</h3>
                                            </div>
                                        </div>
                                        <div class="header-cart-content">
                                            <div class="cart_page_mobile content-product-list">
                                                @foreach($order_items as $cart)
                                                    <div class="item-product item productid-{{ $order_item->id }}">
                                                        <div class="item-product-cart-mobile">
                                                            <a class="product-images1" href="{{ route('product.show', $order_item->product_variant->product->slug) }}"
                                                               title="{{ $order_item->product_variant->product->title }}">
                                                                <img width="80" height="150"
                                                                     src="{{ $order_item->product_variant->product->image }}"
                                                                     alt="{{ $order_item->product_variant->product->title }} - {{ $order_item->product_variant->attr_values }}"/>
                                                            </a>
                                                        </div>
                                                        <div class="title-product-cart-mobile">
                                                            <h3><a href="{{ route('product.show', $order_item->product_variant->product->slug) }}"
                                                                   title="{{ $order_item->product_variant->product->title }}">{{ $order_item->product_variant->product->title }}</a></h3>
                                                            <p>Giá: <span>{{ formatPrice($order_item->price) }}</span></p>
                                                        </div>
                                                        <div class="select-item-qty-mobile">
                                                            <div class="txt_center">
                                                                <input class="variantID" type="hidden" name="variantId"
                                                                       value="{{ $order_item->id }}">
                                                                <input type="text"
                                                                       maxlength="12"
                                                                       min="0"
                                                                       class="input-text number-sidebar qtyMobile{{ $order_item->id }}"
                                                                       id="qtyMobile{{ $order_item->id }}"
                                                                       name="Lines"
                                                                       size="4"
                                                                       readonly
                                                                       value="{{ $order_item->quantity }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="header-cart-price">
                                                <div class="title-cart ">
                                                    <h3 class="text-xs-left">Tổng tiền</h3>
                                                    <a class="text-xs-right totals_price_mobile"> {{ formatPrice($order->total_price) }}</a>
                                                </div>
                                                <div class="checkout">
                                                    <form action="{{ route('checkout.repurchase.item') }}" method="post">
                                                        @csrf
                                                        <input type="hidden" name="variant_id" value="{{ $order_item->product_variant->id }}">
                                                        <input type="hidden" name="product_id" value="{{ $order_item->product_variant->product_id }}">
                                                        <input type="hidden" name="size_id" value="{{ $order_item->product_variant->size_id }}">
                                                        <input type="hidden" name="color_id" value="{{ $order_item->product_variant->color_id }}">
                                                        <input type="hidden" name="product_title" value="{{ $order_item->product_variant->product->title }}">
                                                        <input type="hidden" name="quantity" value="{{ $order_item->quantity }}">
                                                        <input type="hidden" name="product_price" value="{{ $order_item->product_variant->product->price }}">
                                                        <button class="btn-proceed-checkout-mobile"><u>Mua lại</u></button>
                                                    </form>
                                                    <button class="btn btn-primary btn-proceed-continues-mobile"
                                                            title="Tiếp tục mua hàng" type="button"
                                                            onclick="window.location.href='/'">
                                                        Trở về trang chủ
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
