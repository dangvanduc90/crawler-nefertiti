<!DOCTYPE html>
<html class="floating-labels" lang="vi">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="{{ config('app.name') }} - Thanh toán đơn hàng" />
    <title>{{ config('app.name') }} - Thanh toán đơn hàng</title>
    <link rel="shortcut icon" href="https://bizweb.dktcdn.net/100/404/112/themes/787627/assets/favicon.png" type="image/x-icon" />
    <link href='{{ asset('/css/font-awesome-v5.7.2.css') }}' rel='stylesheet'>
    <link href="{{ asset('css/checkout.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/checkout.vendor.min.css') }}" rel="stylesheet">
    <!-- Begin checkout custom css -->
    <style>
        a,
        .radio__label__icon,
        .payment-due__price,
        .spinner--active,
        .icon-print,
        .alert--info,
        .order-summary-toggle__total-recap {
            color: #ef3f3f;
        }
        .input-checkbox:checked, .input-radio:checked {
            -webkit-box-shadow: 0 0 0 10px #ef3f3f inset;
            box-shadow: 0 0 0 10px #ef3f3f inset;
        }
        .product-thumbnail__quantity {
            background-color: #ef3f3f;
        }
        a:hover, a:focus,
        .icon-print:hover {
            color: #B32F2F;
        }
        .field__input:focus,
        .select2-selection:focus, .select2-search__field:focus
        {
            border-color: #ef3f3f;
            box-shadow: 0 0 0 1px #ef3f3f;
        }
        .select2-container--default .select2-results__option--highlighted.select2-results__option--selectable
        {
            background-color: #ef3f3f;
        }
        .btn {
            background-color: #ef3f3f;
            border-color: #ef3f3f;
        }
        .btn:focus {
            outline-color:#ef3f3f;
        }
        .btn.disabled {
            background-color: #F36F6F;
            border-color: #F36F6F;
        }
        .btn:hover {
            background-color: #B32F2F;
            border-color: #B32F2F;
        }
        .btn {
            color: #FFFFFF;
        }
        .product-thumbnail__quantity {
            color: #FFFFFF;
        }
    </style>
    <!-- End checkout custom css -->
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/foot.js') }}"></script>
    <script src="{{ asset('js/checkout.js') }}"></script>
{{--    <script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.0/dist/js.cookie.min.js"></script>--}}
</head>
<body data-no-turbolink>
<header class="banner">
    <div class="wrap">
        <div class="logo logo--center">
            <a href="/">
                <img class="logo__image  logo__image--medium " alt="{{ config('app.name') }}" src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/logo.png?1624697016975" />
            </a>
        </div>
    </div>
</header>
<aside>
    <button class="order-summary-toggle" data-toggle="#order-summary" data-toggle-class="order-summary--is-collapsed">
			<span class="wrap">
				<span class="order-summary-toggle__inner">
					<span class="order-summary-toggle__text expandable">
						Đơn hàng ({{ Cart::count() }} sản phẩm)
					</span>
					<span class="order-summary-toggle__total-recap far fa-angle-down" ></span>
				</span>
			</span>
    </button>
</aside>
<div class="content">
    <form data-tg-refresh="checkout" id="checkoutForm" method="post"
          autocomplete="off"
          data-define="{
				loadingShippingErrorMessage: 'Không thể load phí vận chuyển. Vui lòng thử lại',
				loadingdiscount_codeErrorMessage: 'Có lỗi xảy ra khi áp dụng khuyến mãi. Vui lòng thử lại',
				submitingCheckoutErrorMessage: 'Có lỗi xảy ra khi xử lý. Vui lòng thử lại',
				requireShipping: true,
				requireDistrict: false,
				requireWard: false,
				shouldSaveCheckoutAbandon: true}"
          action="{{ route('order.store') }}"
          data-bind-event-submit="handleCheckoutSubmit(event)"
          data-bind-event-keypress="handleCheckoutKeyPress(event)"
          data-bind-event-change="handleCheckoutChange(event)">
        @csrf
        <div class="wrap">
            <main class="main">
                <header class="main__header">
                    <div class="logo logo--center">
                        <a href="/">
                            <img class="logo__image  logo__image--medium " alt="{{ config('app.name') }}" src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/logo.png?1624697016975" />
                        </a>
                    </div>
                </header>
                <div class="main__content">
                    <article class="animate-floating-labels row">
                        <div class="col col--two">
                            <section class="section">
                                <div class="section__header">
                                    <div class="layout-flex">
                                        <h2 class="section__title layout-flex__item layout-flex__item--stretch">
                                            <i class="fa fa-id-card-o fa-lg section__title--icon hide-on-desktop"></i>
                                            Thông tin nhận hàng
                                        </h2>
                                        <a href="/account/login?returnUrl=/checkout/f1410e902f4c4517b551428947b8dcfd">
                                            <i class="fa fa-user-circle-o fa-lg"></i>
                                            <span>Đăng nhập </span>
                                        </a>
                                    </div>
                                </div>
                                <div class="section__content">
                                    <div class="fieldset">
                                        <div class="field {{ isset($_COOKIE['email']) ? 'field--show-floating-label' : '' }}" data-bind-class="{'field--show-floating-label': email}">
                                            <div class="field__input-wrapper">
                                                <label for="email" class="field__label">
                                                    Email
                                                </label>
                                                <input name="email" id="email"
                                                       type="email" class="field__input"
                                                       data-bind="email" value="{{ $_COOKIE['email'] ?? '' }}">
                                            </div>
                                        </div>
                                        <div class="field {{ isset($_COOKIE['billing_name']) ? 'field--show-floating-label' : '' }}" data-bind-class="{'field--show-floating-label': billing.name}">
                                            <div class="field__input-wrapper">
                                                <label for="billing_name" class="field__label">Họ và tên</label>
                                                <input name="billing_name" id="billing_name"
                                                       type="text" class="field__input"
                                                       data-bind="billing.name" value="{{ $_COOKIE['billing_name'] ?? '' }}">
                                            </div>
                                        </div>
                                        <div class="field {{ isset($_COOKIE['billing_phone']) ? 'field--show-floating-label' : '' }}" data-bind-class="{'field--show-floating-label': billing.phone}">
                                            <div class="field__input-wrapper">
                                                <label for="billing_phone" class="field__label">
                                                    Số điện thoại
                                                </label>
                                                <input name="billing_phone" id="billing_phone"
                                                       type="tel" class="field__input"
                                                       data-bind="billing.phone" value="{{ $_COOKIE['billing_phone'] ?? '' }}">
                                            </div>
                                        </div>
                                        <div class="field {{ isset($_COOKIE['billing_address']) ? 'field--show-floating-label' : '' }}" data-bind-class="{'field--show-floating-label': billing.address}">
                                            <div class="field__input-wrapper">
                                                <label for="billing_address" class="field__label">
                                                    Địa chỉ (tùy chọn)
                                                </label>
                                                <input name="billing_address" id="billing_address"
                                                       type="text" class="field__input"
                                                       data-bind="billing.address" value="{{ $_COOKIE['billing_address'] ?? '' }}">
                                            </div>
                                        </div>
                                        <div class="field field--show-floating-label ">
                                            <div class="field__input-wrapper field__input-wrapper--select2">
                                                <label for="billing_province_id" class="field__label">Tỉnh thành</label>
                                                <select name="billing_province_id" id="billing_province_id"
                                                        size="1"
                                                        class="field__input field__input--select"
                                                        data-bind="billing.province"
                                                        data-address-type="province"
                                                        data-address-zone="billing">
                                                    <option value=""></option>
                                                    @foreach($provinces as $province)
                                                        <option value="{{ $province->id }}">{{ $province->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="field field--show-floating-label "
                                        >
                                            <div class="field__input-wrapper field__input-wrapper--select2">
                                                <label for="billing_district_id" class="field__label">
                                                    Quận huyện (tùy chọn)
                                                </label>
                                                <select name="billing_district_id"
                                                        id="billing_district_id"
                                                        size="1"
                                                        class="field__input field__input--select"
                                                        data-bind="billing.district"
                                                        data-address-type="district"
                                                        data-address-zone="billing">
                                                    <option value=""></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="field field--show-floating-label "
                                        >
                                            <div class="field__input-wrapper field__input-wrapper--select2">
                                                <label for="billing_ward_id" class="field__label">
                                                    Phường xã (tùy chọn)
                                                </label>
                                                <select name="billing_ward_id" id="billing_ward_id"
                                                        size="1"
                                                        class="field__input field__input--select"
                                                        data-bind="billing.ward"
                                                        data-address-type="ward"
                                                        data-address-zone="billing">
                                                    <option value=""></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <div class="fieldset">
                                <h3 class="visually-hidden">Ghi chú</h3>
                                <div class="field " data-bind-class="{'field--show-floating-label': note}">
                                    <div class="field__input-wrapper">
                                        <label for="note" class="field__label">
                                            Ghi chú (tùy chọn)
                                        </label>
                                        <textarea name="note" id="note"
                                                  class="field__input"
                                                  data-bind="note"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col col--two">
                            <section class="section">
                                <div class="section__header">
                                    <div class="layout-flex">
                                        <h2 class="section__title layout-flex__item layout-flex__item--stretch">
                                            <i class="fa fa-truck fa-lg section__title--icon hide-on-desktop"></i>
                                            Vận chuyển
                                        </h2>
                                    </div>
                                </div>
                                <div class="section__content" data-tg-refresh="refreshShipping" id="shippingMethodList"
                                     data-define="{isAddressSelecting: false, shippingMethods: []}">
{{--                                    <div class="alert alert--loader spinner spinner--active" data-bind-show="isLoadingShippingMethod">--}}
{{--                                        <svg xmlns="http://www.w3.org/2000/svg" class="spinner-loader">--}}
{{--                                            <use href="#spinner"></use>--}}
{{--                                        </svg>--}}
{{--                                    </div>--}}
{{--                                    <div class="alert alert-retry alert--danger"--}}
{{--                                         data-bind-event-click="handleShippingMethodErrorRetry()"--}}
{{--                                         data-bind-show="!isLoadingShippingMethod && !isAddressSelecting && isLoadingShippingError">--}}
{{--                                        <span data-bind="loadingShippingErrorMessage"></span> <i class="fa fa-refresh"></i>--}}
{{--                                    </div>--}}
                                    <div class="content-box" data-bind-show="!isLoadingShippingMethod && !isAddressSelecting && !isLoadingShippingError" data-define="{shipping_method: '593151_0,40.000 VND'}">
                                        <div class="content-box__row" data-define-array="{shippingMethods: {name: '593151_0,40.000 VND', textPrice: '40.000₫', textDiscountPrice: '-', subtotalPriceWithShippingFee: '926.000₫'}}">
                                            <div class="radio-wrapper">
                                                <div class="radio__input">
                                                    <input type="checkbox" class="input-radio" {{ isset($_COOKIE['shipping_method']) && $_COOKIE['shipping_method'] == 1 ? 'checked' : '' }}
                                                           name="shipping_method" id="shipping_method"
                                                           value="1"
                                                           data-bind="shipping_method">
                                                </div>
                                                <label for="shipping_method" class="radio__label">
                                                    <span class="radio__label__primary">Giao hàng tận nơi</span>
                                                    <span class="radio__label__accessory">
															<span class="content-box__emphasis price">
																40.000₫
															</span>
														</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
{{--                                    <div class="alert alert--info" data-bind-show="!isLoadingShippingMethod && isAddressSelecting">--}}
{{--                                        Vui lòng nhập thông tin giao hàng--}}
{{--                                    </div>--}}
                                </div>
                            </section>
                            <section class="section">
                                <div class="section__header">
                                    <div class="layout-flex">
                                        <h2 class="section__title layout-flex__item layout-flex__item--stretch">
                                            <i class="fa fa-credit-card fa-lg section__title--icon hide-on-desktop"></i>
                                            Thanh toán
                                        </h2>
                                    </div>
                                </div>
                                <div class="section__content">
                                    <div class="content-box" data-define="{payment_method: undefined}">
                                        <div class="content-box__row">
                                            <div class="radio-wrapper">
                                                <div class="radio__input">
                                                    <input name="payment_method" id="payment_method" {{ isset($_COOKIE['payment_method']) && $_COOKIE['payment_method'] == 1 ? 'checked' : '' }}
                                                           type="checkbox" class="input-radio"
                                                           data-bind="payment_method"
                                                           value="1"
                                                    >
                                                </div>
                                                <label for="payment_method" class="radio__label">
                                                    <span class="radio__label__primary">Chuyển khoản qua ngân hàng</span>
                                                    <span class="radio__label__accessory">
															<span class="radio__label__icon">
																<i class="fas fa-money-bill-alt"></i>
															</span>
														</span>
                                                </label>
                                            </div>
                                            <div class="content-box__row__desc" data-bind-show="payment_method == 494827">
                                                <p>DANH SÁCH TÀI KHOẢN {{ Str::upper(config('app.short_name')) }}
                                                </p> <p>1.Tài khoản mở tại Ngân hàng Á Châu - ACB
                                                </p> <p>Số tài khoản 366358888
                                                </p> <p>Chủ tài khoản : Nguyễn Thị Ngọc Bích - Tài khoản mở tại : Chi Nhánh Thăng Long
                                                </p> <p>2.Số tài khoản: 139888766
                                                </p> <p>Tên tài khoản : Nguyễn Thị Ngọc Bích
                                                </p> <p>Tại: ngân hàng Vpbank CN Thăng Long
                                                </p> <p>3.Tài khoản tại Ngân hàng kỹ thương Việt Nam Chi Nhánh Kim Liên - TECHCOMBANK
                                                </p> <p>Số tài khoản: 103 226 262 030 14
                                                </p> <p>Chủ tài khoản: Nguyễn Thị Ngọc Bích</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </article>
                    <div class="field__input-btn-wrapper field__input-btn-wrapper--vertical hide-on-desktop">
                        <button type="submit" class="btn btn-checkout spinner"
                                data-bind-class="{'spinner--active': isSubmitingCheckout}"
                                data-bind-disabled="isSubmitingCheckout || isLoadingdiscount_code">
                            <span class="spinner-label">ĐẶT HÀNG</span>
                            <svg xmlns="http://www.w3.org/2000/svg" class="spinner-loader">
                                <use href="#spinner"></use>
                            </svg>
                        </button>
                        <a href="{{ route('cart.index') }}" class="previous-link">
                            <i class="previous-link__arrow">❮</i>
                            <span class="previous-link__content">Quay về giỏ hàng</span>
                        </a>
                    </div>
                    <div id="common-alert" data-tg-refresh="refreshError">
                        <div class="alert alert--danger hide-on-desktop"
                             data-bind-show="!isSubmitingCheckout && isSubmitingCheckoutError"
                             data-bind="submitingCheckoutErrorMessage">
                        </div>
                    </div>
                </div>
            </main>
            <aside class="sidebar">
                <div class="sidebar__header">
                    <h2 class="sidebar__title">
                        Đơn hàng ({{ Cart::count() }} sản phẩm)
                    </h2>
                </div>
                <div class="sidebar__content">
                    <div id="order-summary" class="order-summary order-summary--is-collapsed">
                        <div class="order-summary__sections">
                            <div class="order-summary__section order-summary__section--product-list order-summary__section--is-scrollable order-summary--collapse-element">
                                <table class="product-table">
                                    <caption class="visually-hidden">Chi tiết đơn hàng</caption>
                                    <thead class="product-table__header">
                                    <tr>
                                        <th>
                                            <span class="visually-hidden">Ảnh sản phẩm</span>
                                        </th>
                                        <th>
                                            <span class="visually-hidden">Mô tả</span>
                                        </th>
                                        <th>
                                            <span class="visually-hidden">Sổ lượng</span>
                                        </th>
                                        <th>
                                            <span class="visually-hidden">Đơn giá</span>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach(Cart::content() as $cart)
                                            <tr class="product">
                                                <td class="product__image">
                                                    <div class="product-thumbnail">
                                                        <div class="product-thumbnail__wrapper" data-tg-static>
                                                            <img src="{{ $cart->model->product->image }}"
                                                                 alt="" class="product-thumbnail__image">
                                                        </div>
                                                        <span class="product-thumbnail__quantity">{{ $cart->qty }}</span>
                                                    </div>
                                                </td>
                                                <th class="product__description">
                                                    <span class="product__description__name">
                                                        {{ $cart->name }} {{ $cart->model->attr_values }}
                                                    </span>
                                                    @if($cart->model->product->size)
                                                        <span class="product__description__property">{{ $cart->model->product->size->title }}</span>
                                                    @endif
                                                    @if($cart->model->product->color)
                                                        <span class="product__description__property">{{ $cart->model->product->color->title }}</span>
                                                    @endif
                                                    @if($cart->model->product->material)
                                                        <span class="product__description__property">{{ $cart->model->product->material }}</span>
                                                    @endif
                                                </th>
                                                <td class="product__quantity visually-hidden"><em>Số lượng:</em> {{ $cart->qty }}</td>
                                                <td class="product__price">{{ formatPrice($cart->price) }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="order-summary__section order-summary__section--discount-code"
                                 data-tg-refresh="refreshDiscount" id="discountCode">
                                <h3 class="visually-hidden">Mã khuyến mại</h3>
                                <div class="edit_checkout animate-floating-labels">
                                    <div class="fieldset">
                                        <div class="field {{ isset($_COOKIE['discount_code']) ? 'field--show-floating-label' : '' }}">
                                            <div class="field__input-btn-wrapper">
                                                <div class="field__input-wrapper">
                                                    <label for="discount_code" class="field__label">Nhập mã giảm giá</label>
                                                    <input name="discount_code" id="discount_code"
                                                           type="text" class="field__input"
                                                           autocomplete="off"
                                                           data-bind-disabled="isLoadingdiscount_code"
                                                           data-bind-event-keypress="handlediscount_codeKeyPress(event)"
                                                           data-define="{discount_code: null}"
                                                           data-bind="discount_code"
                                                           value="{{ $_COOKIE['discount_code'] ?? '' }}"
                                                    >
                                                </div>
                                                <button class="field__input-btn btn spinner" type="button"
                                                        data-bind-disabled="isLoadingdiscount_code || !discount_code"
                                                        data-bind-class="{'spinner--active': isLoadingdiscount_code, 'btn--disabled': !discount_code}"
                                                        data-bind-event-click="applydiscount_code()">
                                                    <span class="spinner-label">Áp dụng</span>
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="spinner-loader">
                                                        <use href="#spinner"></use>
                                                    </svg>
                                                </button>
                                            </div>
                                            <p class="field__message field__message--error field__message--error-always-show"
                                               data-bind-show="!isLoadingdiscount_code && isLoadingdiscount_codeError"
                                               data-bind="loadingdiscount_codeErrorMessage">
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="order-summary__section order-summary__section--total-lines order-summary--collapse-element"
                                 data-define="{subTotalPriceText: '{{ formatPrice(Cart::total(0, '', '')) }}'}"
                                 data-tg-refresh="refreshOrderTotalPrice" id="orderSummary">
                                <table class="total-line-table">
                                    <caption class="visually-hidden">Tổng giá trị</caption>
                                    <thead>
                                    <tr>
                                        <td><span class="visually-hidden">Mô tả</span></td>
                                        <td><span class="visually-hidden">Giá tiền</span></td>
                                    </tr>
                                    </thead>
                                    <tbody class="total-line-table__tbody">
                                    <tr class="total-line total-line--subtotal">
                                        <th class="total-line__name">
                                            Tạm tính
                                        </th>
                                        <td class="total-line__price">{{ formatPrice(Cart::total(0, '', '')) }}</td>
                                    </tr>
                                    <tr class="total-line total-line--shipping-fee">
                                        <th class="total-line__name">
                                            Phí vận chuyển
                                        </th>
                                        <td class="total-line__price">
                                            40.000₫
                                        </td>
                                    </tr>
                                    </tbody>
                                    <tfoot class="total-line-table__footer">
                                    <tr class="total-line payment-due">
                                        <th class="total-line__name">
                                            <span class="payment-due__label-total">
                                                Tổng cộng
                                            </span>
                                        </th>
                                        <td class="total-line__price">
                                            <span class="payment-due__price">
                                                {{ formatPrice(doubleval(Cart::total(0, '', '')) + 40000) }}
                                            </span>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="order-summary__nav field__input-btn-wrapper hide-on-mobile layout-flex--row-reverse">
                                <button type="submit" class="btn btn-checkout spinner"
                                        data-bind-class="{'spinner--active': isSubmitingCheckout}"
                                        data-bind-disabled="isSubmitingCheckout || isLoadingdiscount_code">
                                    <span class="spinner-label">ĐẶT HÀNG</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" class="spinner-loader">
                                        <use href="#spinner"></use>
                                    </svg>
                                </button>
                                <a href="{{ route('cart.index') }}" class="previous-link">
                                    <i class="previous-link__arrow">❮</i>
                                    <span class="previous-link__content">Quay về giỏ hàng</span>
                                </a>
                            </div>
{{--                            <div id="common-alert-sidebar" data-tg-refresh="refreshError">--}}
{{--                                <div class="alert alert--danger hide-on-mobile"--}}
{{--                                     data-bind-show="!isSubmitingCheckout && isSubmitingCheckoutError"--}}
{{--                                     data-bind="submitingCheckoutErrorMessage">--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </aside>
        </div>
    </form>
    <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
        <symbol id="spinner">
            <svg viewBox="0 0 30 30">
                <circle stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-dasharray="85%"
                        cx="50%" cy="50%" r="40%">
                    <animateTransform attributeName="transform"
                                      type="rotate"
                                      from="0 15 15"
                                      to="360 15 15"
                                      dur="0.7s"
                                      repeatCount="indefinite" />
                </circle>
            </svg>
        </symbol>
    </svg>
</div>
</body>
{{--<script>--}}
{{--    console.log(window.getCookie('email'))--}}
{{--</script>--}}
</html>
