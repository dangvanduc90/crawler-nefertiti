@extends('layouts.app')
@section('title', 'Tin tức')
@section('head')
    <link href='{{ asset('/css/evo-blogs.scss.css') }}' rel='stylesheet'>
@endsection
@section('body')
    {{ Breadcrumbs::render() }}
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="evo-list-blog-page margin-top-30">
                    <h1 class="title-head hidden">Tin tức</h1>
                    <section class="list-blogs blog-main">
                        <div class="row">
                            @foreach($posts as $post)
                            <div class="col-md-12 col-sm-12 col-xs-12 clearfix">
                                <article class="blog-item">
                                    <div class="blog-item-thumbnail" data-aos="fade-up">
                                        <a href="{{ route('post.show', $post->slug) }}" title="{{ $post->title }}">
                                            <img src="{{ $post->image }}" data-src="{{ $post->image }}" alt="{{ $post->title }}" class="lazy img-responsive center-block" />
                                        </a>
                                    </div>
                                    <div class="blog-item-mains" data-aos="fade-down">
                                        <div class="post-time">Tuesday 02, tháng 3, 2021</div>
                                        <h3 class="blog-item-name"><a href="{{ route('post.show', $post->slug) }}" title="{{ $post->title }}">{{ $post->title }}</a></h3>
                                        <p class="blog-item-summary margin-bottom-5">{{ $post->description }}</p>
                                    </div>
                                </article>
                            </div>
                            @endforeach
                            {{ $posts->links() }}
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection
