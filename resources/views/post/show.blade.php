@extends('layouts.app')
@section('title', 'Tin tức')
@section('head')
    <link href='{{ asset('/css/evo-article.scss.css') }}' rel='stylesheet'>
@endsection
@section('body')
    {{ Breadcrumbs::render() }}
    <div class="container article-wraper margin-top-20">
        <div class="row">
            <section class="right-content col-md-9">
                <article class="article-main">
                    <div class="row">
                        <div class="col-md-12 evo-article margin-bottom-10">
                            <h1 class="title-head">{{ $post->title }}</h1>
                            <div class="article-details evo-toc-content">
                                {!! $post->content !!}
                            </div>
                        </div>
                    </div>
                </article>
            </section>
            <aside class="evo-toc-sidebar evo-sidebar sidebar left-content col-md-3">
                <aside class="aside-item collection-category">
                    <div class="aside-title">
                        <h3 class="title-head margin-top-0">Danh mục</h3>
                    </div>
                    <div class="aside-content">
                        <ul class="nav navbar-pills nav-category">
                            @foreach($categories as $category)
                            <li class="nav-item ">
                                <a class="nav-link" href="{{ route('category.show', $category->slug) }}" title="{{ $category->title }}">{{ $category->title }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </aside>
                <aside class="aside-item top-news margin-top-20">
                    <div class="aside-title">
                        <h3 class="title-head margin-top-0"><a href="tin-tuc" title="Bài viết xem nhiều">Bài viết xem
                                nhiều</a></h3>
                    </div>
                    <ul class="listpost">
                        @foreach($relatePosts as $key => $post_item)
                        <li class="clearfix">
                            <label>{{ $key + 1 }}</label>
                            <div class="colu">
                                <a href="{{ route('post.show', $post_item->slug) }}"
                                   title="{{ $post_item->title }}"
                                   class="linktitle">{{ $post_item->title }}</a>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </aside>
                <aside class="aside-item blog-banner margin-top-30">
                    <a href="#" title="{{ config('app.name') }}" class="single_image_effect">
                        <img
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"
                            data-src="//bizweb.dktcdn.net/100/404/112/themes/787627/assets/blog_banner.jpg?1624697016975"
                            alt="{{ config('app.name') }}" class="lazy img-responsive center-block"/>
                    </a>
                </aside>
            </aside>
        </div>
    </div>
    </div>
    </div>
@endsection
