@servers(['local' => '127.0.0.1', 'my-server' => ['ubuntu@18.142.21.7']])

@setup
$branch = 'master';
$release = '/var/www/laravel';
@endsetup

@story('deploy', ['on' => 'my-server'])
git
install
@endstory

@task('git')
cd {{ $release }}
git pull origin {{ $branch }}
@endtask

@task('install')
cd {{ $release }}
composer install --no-interaction
php ./artisan migrate --force
@endtask

@finished
echo "Finished!!!!";
@endfinished
