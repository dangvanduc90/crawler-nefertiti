<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\DistrictController;
use App\Http\Controllers\WardController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\SearchController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('tat-ca-san-pham', [ProductController::class, 'index'])->name('product.index');
Route::get('thoi-trang-nam', [ProductController::class, 'forMen'])->name('product.for.men');
Route::get('sale-off', [ProductController::class, 'saleOff'])->name('product.sale.off');
Route::get('dong-gia-699', [ProductController::class, 'saleOff'])->name('product.sale.off');
Route::get('sale-off-50', [ProductController::class, 'saleOff'])->name('product.sale.off');
Route::get('dong-gia-499', [ProductController::class, 'saleOff'])->name('product.sale.off');
Route::get('dong-gia-599', [ProductController::class, 'saleOff'])->name('product.sale.off');
Route::get('dong-gia-399', [ProductController::class, 'saleOff'])->name('product.sale.off');
Route::get('dong-gia-299', [ProductController::class, 'saleOff'])->name('product.sale.off');
Route::get('dong-gia-199', [ProductController::class, 'saleOff'])->name('product.sale.off');
Route::get('dong-gia-99', [ProductController::class, 'saleOff'])->name('product.sale.off');

Route::get('san-pham/{product:slug}', [ProductController::class, 'show'])->name('product.show')
    ->missing(function (Request $request) {
    abort(404);
});
Route::get('danh-muc/{category:slug}', [CategoryController::class, 'show'])->name('category.show')
    ->missing(function (Request $request) {
    abort(404);
});
Route::resource('gio-hang', CartController::class)->except([
    'show', 'create', 'edit'
])->names('cart');
Route::post('gio-hang/mua-lai/item', [CartController::class, 'repurchaseItem'])->name('checkout.repurchase.item');
Route::post('gio-hang/mua-lai/order', [CartController::class, 'repurchaseOrder'])->name('checkout.repurchase.order');

Route::get('thanh-toan', [CheckoutController::class, 'index'])->name('checkout.index');

Route::post('don-hang', [OrderController::class, 'store'])->name('order.store');
Route::get('don-hang/{order:code}', [OrderController::class, 'show'])->name('order.show');
Route::get('cam-on/{order:code}', [OrderController::class, 'thankYou'])->name('order.thank.you');

Route::get('district/{province_id}', [DistrictController::class, 'getDistrictByProvinceId'])->name('district.by.province.id');
Route::get('ward/{district_id}', [WardController::class, 'getWardByDistrictId'])->name('ward.by.district.id');

Route::get('tin-tuc', [PostController::class, 'index'])->name('post.index');
Route::get('tin-tuc/{post:slug}', [PostController::class, 'show'])->name('post.show');

Route::get('tim-kiem-nhanh', [SearchController::class, 'index'])->name('search');
