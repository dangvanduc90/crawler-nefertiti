Number.prototype.formatMoney = function(c, d, t){
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 0 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "." : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "") + '₫';
};

Number.prototype.floatToString = function (e, t) {
    var r = e.toFixed(t).toString();
    return r.match(/^\.\d+/) ? "0" + r : r
}

window.formatDropdown = function (datas = []) {
    let html = '<option value=""></option>'
    datas.forEach(function (data) {
        html += `<option value="${data.id}">${data.name}</option>`
    })
    return html
}

window.getCookie = function(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

window.setCookie = function (cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

$(document).on('click', ".remove-item-cart", function () {
    var rowId = $(this).attr('data-id');
    if( confirm('Are you sure?') ) {
        onChangeItemCart(rowId);
    }
});
$(document).on('click', ".items-count", function () {
    $(this).prop('disabled', true);
    var rowId = $(this).parent().find('.variantID').val();
    var qty =  $(this).parent().children('.number-sidebar').val();
    onChangeItemCart(rowId, qty, $(this));
});
$(document).on('change', ".number-sidebar", function () {
    var rowId = $(this).parent().children('.variantID').val();
    var qty =  $(this).val();
    onChangeItemCart(rowId, qty);
});
function onChangeItemCart (rowId, quantity = 0, container = null){
    const _token = $("meta[name='csrf-token']").attr("content");
    const url = '/gio-hang/' + rowId
    $.ajax({
        url: url,
        data: {
            _token,
            quantity
        },
        type: quantity > 0 ? 'PUT' : 'DELETE',
        dataType: "json",
        success: function (response) {
            if (response.success_flag === true) {
                if (quantity == 0) {
                    $(`.productid-${rowId}`).remove()
                }
                $(`#qty${rowId}`).val(quantity)
                $(`#qtyItem${rowId}`).val(quantity)
                caclCountCart()
                caclTotalCart()
                // caclSubTotalCart()
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(xhr.statusText + "\r\n" + xhr.responseText);
        }
    }).done(function () {
        if (quantity > 0) {
            container.prop('disabled', false);
        }
    })
}

function caclTotalCart() {
    let total = 0
    $('.list-item-cart').each(function () {
        const itemPrice = $(this).find('.pricechange').data('price')
        const itemQuantity = $(this).find('.number-sidebar').val()
        const itemValue = parseFloat(itemPrice) * parseInt(itemQuantity)
        total += itemValue
    })
    $('.totals_price').text(Number(total).formatMoney())
    $('.top-subtotal .price').text(Number(total).formatMoney())
}

function caclCountCart() {
    let count = 0
    $('.quantity-select').each(function () {
        const itemQuantity = $(this).find('.number-sidebar').val()
        count += parseInt(itemQuantity)
    })
    $('.count_item_pr').text(count)
}


// function caclSubTotalCart() {
//
// }
