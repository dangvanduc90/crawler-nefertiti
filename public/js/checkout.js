$(function() {
    $("body").on("click", "[data-toggle]", function() {
        var n = $(this);
        n.toggleClass("toggled");
        $(n.attr("data-toggle")).toggleClass(n.attr("data-toggle-class"))
    }).on("focus", ".field__label+input.field__input,textarea.field__input", function() {
        $(this).closest(".field").addClass("field--show-floating-label")
    }).on("blur", ".field__label+input.field__input,textarea.field__input", function() {
        this.value || $(this).closest(".field").removeClass("field--show-floating-label")
    }).on("change", ".field.field--error  .field__input", function() {
        $(this).closest(".field").removeClass("field--error")
    }).on("keypress", "#checkoutForm input", function(n) {
        n.keyCode == 13 && n.preventDefault()
    }).on("click", "#differenceAddress", function(n) {
        setContactFormTabIndex(n.target.checked)
    }).on("change", "#billing_province_id", function(n) {
        const province_id = $(this).val()
        $.ajax({
            url: '/district/' + province_id,
            data: {
                province_id
            },
            type: 'GET',
            dataType: "json",
            success: function (response) {
                if (response.success_flag === true) {
                    $('#billing_district_id').empty()
                    $('#billing_district_id').append(window.formatDropdown(response.data))
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.statusText + "\r\n" + xhr.responseText);
            }
        })
    }).on("change", "#billing_district_id", function(n) {
        const district_id = $(this).val()
        $.ajax({
            url: '/ward/' + district_id,
            data: {
                district_id
            },
            type: 'GET',
            dataType: "json",
            success: function (response) {
                if (response.success_flag === true) {
                    $('#billing_ward_id').empty()
                    $('#billing_ward_id').append(window.formatDropdown(response.data))
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.statusText + "\r\n" + xhr.responseText);
            }
        })
    })
    $(document).keyup(function(n) {
        n.which === 27 && $(".modal-wrapper").addClass("hide")
    }).on("page:load", function() {
        checkErrorToScroll()
    })
});
$(function() {
    function u() {
        const n = /iPhone|iPad|iPod/.test(navigator.userAgent) && !window.MSStream;
        n || $("select[data-address-type]").select2({
            width: "100%",
            language: {
                noResults: function() {
                    return "Không tìm thấy kết quả"
                }
            }
        })
    }
});
